var tsu;
(function (tsu) {
    let enums;
    (function (enums) {
        class telopt {
        }
        telopt.ENVIRON = 36;
        telopt.GMCP = 201;
        telopt.SE = 240;
        telopt.NOP = 241;
        telopt.DATAMARK = 242;
        telopt.BREAK = 243;
        telopt.INTERRUPTPROCESS = 244;
        telopt.ABORTOUTPUT = 245;
        telopt.AREYOUTHERE = 246;
        telopt.ERASECHARACTER = 247;
        telopt.ERASELINE = 248;
        telopt.GOAHEAD = 249;
        telopt.SB = 250;
        telopt.WILL = 251;
        telopt.WONT = 252;
        telopt.DO = 253;
        telopt.DONT = 254;
        telopt.IAC = 255;
        enums.telopt = telopt;
        class environ {
        }
        environ.IS = 0;
        environ.SEND = 1;
        environ.INFO = 2;
        environ.VAR = 0;
        environ.VALUE = 1;
        environ.ESC = 2;
        environ.USERVAR = 3;
        environ.IP = 5;
        enums.environ = environ;
    })(enums = tsu.enums || (tsu.enums = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    class parseengine {
        constructor() {
            this.chunk = null; //this is a byte array
            this.recording = false;
            this.socket = null;
            this.opt_parsers = [];
            this.string_parsers = [];
            this.pre_parsers = [];
            var $this = this;
            //add parsers for processing
            this.pre_parsers.push(new tsu.parsehandler.relog());
            //telopt
            this.opt_parsers.push(new tsu.parsehandler.gmcp());
            this.opt_parsers.push(new tsu.parsehandler.environ());
            //string
            this.string_parsers.push(new tsu.parsehandler.matcher_color());
            this.string_parsers.push(new tsu.parsehandler.filter_color());
            this.string_parsers.push(new tsu.parsehandler.vote());
            this.string_parsers.push(new tsu.parsehandler.login());
            this.string_parsers.push(new tsu.parsehandler.prompt());
        }
        flush() {
            //flush the data to the browser
            var $this = this;
            if (!$this.chunk || $this.chunk.length == 0)
                return;
            var splicecount = $this.chunk.length;
            var data = this.parsechunk();
            //console.log(data);
            if (data === false)
                return; //we couldn't parse the data yet
            var r = Buffer.from(data.stringdata).toString();
            r = tsu.helpers.cleanString(r);
            this.pre_parsers.forEach(function (v, i, a) {
                r = v.process($this.socket, r);
            });
            if (data.negstructs)
                this.opt_parsers.forEach(function (v, i, a) {
                    //loop optcode negotiations
                    for (var j = 0; j < data.negstructs.length; j++) {
                        v.processnegotiation($this.socket, data.negstructs[j]);
                    }
                });
            if (data.optdatastructs)
                this.opt_parsers.forEach(function (v, i, a) {
                    //loop optcode data
                    for (var j = 0; j < data.optdatastructs.length; j++) {
                        v.processdata($this.socket, data.optdatastructs[j]);
                    }
                });
            this.string_parsers.forEach(function (v, i, a) {
                //run parsers on are data string 
                r = v.process($this.socket, r);
            });
            $this.complete(r);
            $this.chunk.splice(0, splicecount);
        }
        complete(d) {
            this.socket.emit("data", d);
        }
        parsechunk() {
            var r = false;
            var opentelopt = this.getsequence(tsu.enums.telopt.IAC, tsu.enums.telopt.SB);
            var closetelopt = this.getsequence(tsu.enums.telopt.IAC, tsu.enums.telopt.SE);
            if (opentelopt.length == closetelopt.length) {
                r = {
                    stringdata: [],
                    negstructs: [],
                    optdatastructs: []
                };
                r.stringdata = this.chunk;
                var negtelopt = this.getsequencearray(tsu.enums.telopt.IAC, [tsu.enums.telopt.WILL, tsu.enums.telopt.WONT, tsu.enums.telopt.DO, tsu.enums.telopt.DONT]);
                if (negtelopt.length > 0) {
                    for (var i = negtelopt.length - 1; i >= 0; i--) {
                        //negotiations are 3 bytes
                        var index = negtelopt[i];
                        var n = {
                            type: null,
                            action: null,
                            data: []
                        };
                        n.type = r.stringdata[index + 2];
                        n.action = r.stringdata[index + 1];
                        for (var j = index; j <= index + 2; j++) {
                            n.data.push(r.stringdata[j]);
                        }
                        r.stringdata.splice(index, 3);
                        r.negstructs.push(n);
                    }
                }
                if (opentelopt.length > 0 && closetelopt.length > 0) {
                    for (var i = opentelopt.length - 1; i >= 0; i--) {
                        var open_index = opentelopt[i];
                        var close_index = closetelopt[i];
                        var o = {
                            type: null,
                            action: null,
                            data: []
                        };
                        o.type = r.stringdata[open_index + 2];
                        o.action = r.stringdata[open_index + 1];
                        for (var j = open_index + 3; j < close_index; j++) {
                            o.data.push(r.stringdata[j]);
                        }
                        r.stringdata.splice(open_index, (close_index + 1) - open_index);
                        //console.log(o);
                        r.optdatastructs.push(o);
                    }
                }
            }
            else {
                // console.log(opentelopt);
                // console.log(closetelopt);
            }
            return r;
        }
        getsequence(one, two) {
            var $this = this;
            var a = [];
            var sequencetwo = [];
            for (var i = 0; i < $this.chunk.length; i++) {
                if (i + 1 < $this.chunk.length) {
                    sequencetwo = [
                        $this.chunk[i],
                        $this.chunk[i + 1]
                    ];
                    if (sequencetwo[0] == one && sequencetwo[1] == two) {
                        a.push(i);
                    }
                }
            }
            return a;
        }
        getsequencearray(one, two) {
            var $this = this;
            var a = [];
            two.forEach(function (v, i, arr) {
                var b = $this.getsequence(one, v);
                b.forEach(function (v, i, arr2) {
                    a.push(v);
                });
            });
            return a;
        }
    }
    tsu.parseengine = parseengine;
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    class server {
        constructor() {
            this.port = 8004;
            var $this = this;
            //variables setup
            this.express = require('express');
            this.webapp = this.express();
            this.path = require('path');
            this.webserver = require('http').Server(this.webapp);
            this.socketio = require('socket.io')(this.webserver);
            var Net = require('net');
            //start the server
            this.webapp.use('/', this.express.static(__dirname + '/../'));
            this.webserver.listen(this.port, '0.0.0.0', function () {
                console.log('Listening on ' + $this.webserver.address().port);
            });
            //Handle a SOCKETIO error
            this.socketio.on('error', function (socket) {
                socket.tcpclient.destroy();
            });
            //This handles a new connection and setting it up
            this.socketio.on('connection', function (socket) {
                //set a new client on the socket object
                if (socket.tcpclient)
                    socket.tcpclient.destroy();
                //setup new variables for this socket
                socket.tcpclient = new Net.Socket();
                socket.tcpclient.setNoDelay(true);
                socket.loggedin = false;
                socket.tcpclient.socket = socket;
                socket.SP = new tsu.parseengine();
                socket.SP.chunk = [];
                socket.SP.socket = socket;
                socket.queue = [];
                socket.relog = false;
                var options = {
                    hostname: 'tsunami.thebigwave.net',
                    port: 23
                    //localAddress : '192.168.0.100',
                    //localPort: portstart
                };
                //socket handle incoming client data
                socket.tcpclient.on('data', function (chunk) {
                    //console.log(chunk.toString() + "\n\n");
                    try {
                        socket.queue.push(chunk);
                    }
                    catch (err) {
                        console.log(err);
                    }
                });
                function connect() {
                    //make a new connection
                    socket.tcpclient.connect(23, 'thebigwave.net', function () {
                        //console.log(console.log(options));      
                        console.log('A new TCP connection established with the server.');
                    });
                }
                socket.logout = function () {
                    socket.loggedin = false;
                    socket.relog = false;
                    //connect();
                    try {
                        socket.emit("logout");
                    }
                    catch (e) { }
                };
                //close the tcp socket
                socket.tcpclient.on('close', function (e) {
                    socket.loggedin = false;
                    console.log('Requested an end to the TCP connection');
                    console.log(e);
                    socket.logout();
                    //todo EMIT NOT READY SIGNAL
                    //connect();
                });
                //handle client tcp error
                socket.tcpclient.on('error', function (e) {
                    console.log(e);
                    socket.logout();
                });
                connect();
                //queue for process stuff for this socket
                socket.processqueue = function () {
                    socket.queue.forEach(function (v, i, a) {
                        v.forEach(function (c) {
                            socket.SP.chunk.push(c); //we convert the chunk into our byte array, so we can splice it up
                        });
                        socket.SP.flush(socket);
                    });
                    socket.queue = [];
                    setTimeout(function () {
                        socket.processqueue();
                    }, 1);
                };
                socket.processqueue();
                //get user input
                socket.on('input', function (input) {
                    // if(data == "gmcp1") {
                    //future alias commands
                    // }
                    var data = input.trim();
                    if (data === "logout") {
                        socket.logout();
                    }
                    data += "\n";
                    //console.log(data);
                    //socket.emit("input",data);
                    socket.tcpclient.write(data);
                });
                socket.on('test', function () {
                    console.log('Client just requested test.');
                    this.socketio.emit('test', "test");
                });
                //todo EMIT READY SIGNAL
            });
        }
    }
    tsu.server = server;
})(tsu || (tsu = {}));
var tsunamiui_server = new tsu.server(); //this starts the server
var tsu;
(function (tsu) {
    let parsehandler;
    (function (parsehandler) {
        class filter_color {
            process(socket, input) {
                return input.replace(/\[color=.*?\]/g, "").replace(/\[\/color\]/g, "");
                ;
            }
        }
        parsehandler.filter_color = filter_color;
    })(parsehandler = tsu.parsehandler || (tsu.parsehandler = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    let parsehandler;
    (function (parsehandler) {
        class login {
            process(socket, input) {
                if (input.toLowerCase().includes('thanks for supporting your favorite mud')) { //todo this could be alot better
                    //we are logged in now
                    // socket.tcpclient.write((<any>Buffer).from([enums.telopt.IAC, enums.telopt.SB, enums.telopt.GMCP].concat(helpers.toArrayBuffer((<any>Buffer).from("char.vitals","binary"))).concat([enums.telopt.IAC, enums.telopt.SE]), "binary"));
                    // socket.tcpclient.write((<any>Buffer).from([enums.telopt.IAC, enums.telopt.SB, enums.telopt.GMCP].concat(helpers.toArrayBuffer((<any>Buffer).from("char.items","binary"))).concat([enums.telopt.IAC, enums.telopt.SE]), "binary"));
                    // socket.tcpclient.write((<any>Buffer).from([enums.telopt.IAC, enums.telopt.SB, enums.telopt.GMCP].concat(helpers.toArrayBuffer((<any>Buffer).from("char.status","binary"))).concat([enums.telopt.IAC, enums.telopt.SE]), "binary"));
                    // socket.tcpclient.write((<any>Buffer).from([enums.telopt.IAC, enums.telopt.SB, enums.telopt.GMCP].concat(helpers.toArrayBuffer((<any>Buffer).from("area","binary"))).concat([enums.telopt.IAC, enums.telopt.SE]), "binary"));
                    // socket.tcpclient.write((<any>Buffer).from([enums.telopt.IAC, enums.telopt.SB, enums.telopt.GMCP].concat(helpers.toArrayBuffer((<any>Buffer).from("room","binary"))).concat([enums.telopt.IAC, enums.telopt.SE]), "binary"));
                    socket.isloggedin = true;
                    socket.emit("loggedin");
                    console.log("A user has logged in");
                    setTimeout(function () {
                        if (!socket.relog) {
                            console.log("Sending GMCP commands");
                            socket.tcpclient.write("gmcp enable char\n");
                            socket.tcpclient.write("gmcp enable room\n");
                            socket.tcpclient.write("gmcp enable area\n");
                        }
                        else {
                            console.log("The character is already logged in");
                        }
                    }, 3000);
                }
                return input;
            }
        }
        parsehandler.login = login;
    })(parsehandler = tsu.parsehandler || (tsu.parsehandler = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    let parsehandler;
    (function (parsehandler) {
        class matcher_color {
            process(socket, input) {
                /*
                JSON.stringify(`${d}`).replace(/.*?(\\u.*?m)(.*?)(\\u.*?m).*?/g, <any>function(match, $1, $2, $3, offset, original) {
                    //if(!($1 && $2 && $3)) return;
                    return; //todo get this color thing to parse correctly
                    var color = {
                    colorstart:$1
                    ,val:$2
                    ,colorend:$3
                    }
                    console.log(JSON.stringify(color));
                    $this.socket.emit("oncolor",JSON.stringify(color));
                    return;
                });
                */
                return input;
            }
        }
        parsehandler.matcher_color = matcher_color;
    })(parsehandler = tsu.parsehandler || (tsu.parsehandler = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    let parsehandler;
    (function (parsehandler) {
        class prompt {
            process(socket, input) {
                var lines = input.match(/[^\r\n]+/g);
                var isprompt = false;
                if (lines)
                    lines.forEach(function (l) {
                        if (l.trim() == ">") {
                            isprompt = true;
                            socket.emit("onprompt", input);
                        }
                    });
                if (!isprompt && input.trim().length > 0)
                    socket.emit("ondata", input);
                return input;
            }
        }
        parsehandler.prompt = prompt;
    })(parsehandler = tsu.parsehandler || (tsu.parsehandler = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    let parsehandler;
    (function (parsehandler) {
        class relog {
            process(socket, input) {
                if (input.includes("Throw the other copy out? [y/n]")) {
                    socket.relog = true;
                }
                return input;
            }
        }
        parsehandler.relog = relog;
    })(parsehandler = tsu.parsehandler || (tsu.parsehandler = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    let parsehandler;
    (function (parsehandler) {
        class vote {
            process(socket, input) {
                if (input.includes('http://bit.ly/'))
                    socket.emit('vote');
                return input;
            }
        }
        parsehandler.vote = vote;
    })(parsehandler = tsu.parsehandler || (tsu.parsehandler = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    let parsehandler;
    (function (parsehandler) {
        class environ {
            sendip(socket) {
                if (socket.isusingenviron && socket.isusingenviron == 1) {
                    var ip = socket.request.headers['x-real-ip'] || socket.request.connection.remoteAddress;
                    socket.tcpclient.write(Buffer.from([tsu.enums.telopt.IAC, tsu.enums.telopt.SB, tsu.enums.telopt.ENVIRON], "binary"));
                    socket.tcpclient.write(`${tsu.enums.environ.IS} ${tsu.enums.environ.VAR} ${tsu.enums.environ.IP} ${tsu.enums.environ.VALUE} ${ip}`);
                    socket.tcpclient.write(Buffer.from([tsu.enums.telopt.IAC, tsu.enums.telopt.SE], "binary"));
                }
            }
            processdata(socket, input) {
                if (input.type != tsu.enums.telopt.ENVIRON)
                    return;
                //console.log("Getting Send Request");
                console.log(input);
                //IAC SB ENVIRON SEND VAR IP IAC SE
                try {
                    if (input.data[0] == tsu.enums.environ.SEND) {
                        for (var i = 1; i < input.data.length; i++) {
                            if (input.data[i] == tsu.enums.environ.VAR) {
                                if (i + 1 >= input.data.length)
                                    continue;
                                //todo this might need to change in the future to handle string variable names like "IP" instead of custom enum byte values
                                switch (input.data[i + 1]) {
                                    case tsu.enums.environ.IP:
                                        //console.log(socket.request.headers);
                                        //console.log(socket.request.connection);
                                        this.sendip(socket);
                                        break;
                                }
                            }
                        }
                    }
                }
                catch (e) {
                    console.log(e);
                }
            }
            processnegotiation(socket, input) {
                if (input.type != tsu.enums.telopt.ENVIRON)
                    return;
                if (input.action == tsu.enums.telopt.DO) {
                    console.log("Sending WILL ENVIRON for this socket");
                    //IAC WILL ENVIRON, we do ENVIRON
                    socket.isusingenviron = 1;
                    socket.tcpclient.write(Buffer.from([tsu.enums.telopt.IAC, tsu.enums.telopt.WILL, tsu.enums.telopt.ENVIRON], "binary"));
                    //socket.tcpclient.write((<any>Buffer).from([enums.telopt.IAC, enums.telopt.SB, enums.telopt.ENVIRON, enums.environ.IS, enums.environ.VAR].concat(helpers.toArrayBuffer((<any>Buffer).from("\"IP\"","binary"))).concat([enums.environ.VALUE]).concat(helpers.toArrayBuffer((<any>Buffer).from("\""+socket.handshake.address+"\"","binary"))).concat([enums.telopt.IAC, enums.telopt.SE]), "binary"));   
                }
            }
        }
        parsehandler.environ = environ;
    })(parsehandler = tsu.parsehandler || (tsu.parsehandler = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    let parsehandler;
    (function (parsehandler) {
        class gmcp {
            processdata(socket, input) {
                if (input.type != tsu.enums.telopt.GMCP)
                    return;
                //The server is sending us a JSON object of data to handle
                var jsonbytes = [];
                var fc = new parsehandler.filter_color();
                var json = fc.process(socket, Buffer.from(input.data).toString());
                //console.log(json);
                if (json.length > 0) {
                    json = tsu.helpers.cleanString(json).trim();
                    var name = json.substr(0, json.indexOf('{') - 1).trim();
                    var j = json.substr(json.indexOf('{') - 1).substr(0, json.lastIndexOf("}")).trim();
                    try {
                        JSON.parse(j);
                        socket.emit(name, j);
                    }
                    catch (e) {
                        console.log("ERROR: " + name);
                    }
                }
            }
            processnegotiation(socket, input) {
                if (input.type != tsu.enums.telopt.GMCP)
                    return;
                if (input.action == tsu.enums.telopt.WILL) {
                    //IAC DO GMCP, we do GMCP
                    console.log("Sending DO GMCP for this socket");
                    socket.tcpclient.write(Buffer.from([tsu.enums.telopt.IAC, tsu.enums.telopt.SB, tsu.enums.telopt.GMCP].concat(tsu.helpers.toArrayBuffer(Buffer.from("enable gmcp", "binary"))).concat([tsu.enums.telopt.IAC, tsu.enums.telopt.SE]), "binary"));
                }
            }
        }
        parsehandler.gmcp = gmcp;
    })(parsehandler = tsu.parsehandler || (tsu.parsehandler = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    let enums;
    (function (enums) {
        class events {
        }
        events.vote = 'vote';
        events.tab_move = 'tabmove';
        events.area = 'area';
        events.room = 'room';
        events.logout = 'logout';
        events.loggedin = 'loggedin';
        events.char_items = 'char.items';
        events.char_vitals = 'char.vitals';
        events.char_status = 'char.status';
        events.onprompt = 'onprompt';
        events.ondata = 'ondata';
        events.oncolor = 'oncolor';
        events.afterload_socket = 'afterload_socket';
        events.after_login = 'afterload_login';
        events.after_logout = 'afterload_login';
        events.afterload_terminal = 'afterload_terminal';
        events.afterload_database = 'afterload_database';
        events.afterload_elementengine = 'afterload_elementengine';
        enums.events = events;
    })(enums = tsu.enums || (tsu.enums = {}));
})(tsu || (tsu = {}));
var tsu;
(function (tsu) {
    class helpers {
        static cleanString(input) {
            var output = "";
            for (var i = 0; i < input.length; i++) {
                if (input.charCodeAt(i) <= 127) {
                    output += input.charAt(i);
                }
            }
            return output;
        }
        static toArrayBuffer(buf) {
            var ab = new ArrayBuffer(buf.length);
            var view = new Uint8Array(ab);
            for (var i = 0; i < buf.length; ++i) {
                view[i] = buf[i];
            }
            return ab;
        }
        static isMobile() {
            var isMobile = {
                Android: function () {
                    return navigator.userAgent.match(/Android/i);
                },
                BlackBerry: function () {
                    return navigator.userAgent.match(/BlackBerry/i);
                },
                iOS: function () {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
                Opera: function () {
                    return navigator.userAgent.match(/Opera Mini/i);
                },
                Windows: function () {
                    return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
                },
                any: function () {
                    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                }
            };
            return isMobile.any();
        }
    }
    tsu.helpers = helpers;
})(tsu || (tsu = {}));
//# sourceMappingURL=server.js.map