namespace tsu {
    export class parseengine {
        public chunk = null; //this is a byte array
        public recording = false;
        public socket = null;
        public opt_parsers : parsehandler.itelopthandler[] = [];
        public string_parsers : parsehandler.iparsehandler[] = [];
        public pre_parsers : parsehandler.iparsehandler[] = [];
        public constructor(){
            var $this = this;

            //add parsers for processing
            this.pre_parsers.push( new parsehandler.relog() );
            
            //telopt
            this.opt_parsers.push( new parsehandler.gmcp() );
            this.opt_parsers.push( new parsehandler.environ() );

            //string
            this.string_parsers.push( new parsehandler.matcher_color() );
            this.string_parsers.push( new parsehandler.filter_color() );
            this.string_parsers.push( new parsehandler.vote() );
            this.string_parsers.push( new parsehandler.login() );
            this.string_parsers.push( new parsehandler.prompt() );
        }
        public flush(){
            //flush the data to the browser
            var $this = this;
            if(!$this.chunk || $this.chunk.length == 0) return;
            var splicecount = $this.chunk.length;

            var data = <any>this.parsechunk();
            //console.log(data);
            if(data === false) return; //we couldn't parse the data yet
            var r = (<any>Buffer).from(data.stringdata).toString();
            r = helpers.cleanString(r);

            this.pre_parsers.forEach(function(v,i,a){
                r = v.process( $this.socket, r );
            });

            if(data.negstructs)
                this.opt_parsers.forEach(function(v,i,a){
                    //loop optcode negotiations
                    for(var j=0; j<data.negstructs.length; j++){
                        v.processnegotiation( $this.socket, data.negstructs[j] );
                    }
                });

            if(data.optdatastructs)
                this.opt_parsers.forEach(function(v,i,a){
                    //loop optcode data
                    for(var j=0; j<data.optdatastructs.length; j++){
                        v.processdata( $this.socket, data.optdatastructs[j] );
                    }
                });            
            
            this.string_parsers.forEach(function(v,i,a){
                //run parsers on are data string 
                r = v.process( $this.socket, r );
            });

            $this.complete(r);
            $this.chunk.splice(0,splicecount);
        }
        public complete(d:string) : void{
            this.socket.emit("data",d);
        }
        public parsechunk() : boolean | object{
            var r : any = false;
            var opentelopt = this.getsequence(enums.telopt.IAC, enums.telopt.SB);
            var closetelopt = this.getsequence(enums.telopt.IAC, enums.telopt.SE);
            if(opentelopt.length == closetelopt.length) {
                r = {
                    stringdata : []
                    ,negstructs : []
                    ,optdatastructs : []
                };
                r.stringdata = this.chunk;
                var negtelopt = this.getsequencearray(enums.telopt.IAC, [enums.telopt.WILL,enums.telopt.WONT,enums.telopt.DO,enums.telopt.DONT]);
                if(negtelopt.length > 0){
                    for(var i=negtelopt.length-1; i>=0;i--){
                        //negotiations are 3 bytes
                        var index = negtelopt[i];
                        var n = {
                            type:null
                            ,action:null
                            ,data:[]
                        }
                        n.type = r.stringdata[index+2];
                        n.action = r.stringdata[index+1];
                        for(var j = index; j <= index+2; j++){
                            n.data.push(r.stringdata[j]);
                        }
                        r.stringdata.splice(index,3);
                        r.negstructs.push(n);
                    }
                }
                if(opentelopt.length > 0 && closetelopt.length > 0){
                    for(var i=opentelopt.length-1; i>=0;i--){                      
                        var open_index = opentelopt[i];
                        var close_index = closetelopt[i];
                        var o = {
                            type:null
                            ,action:null
                            ,data:[]
                        }
                        o.type = r.stringdata[open_index+2];
                        o.action = r.stringdata[open_index+1];
                        for(var j = open_index+3; j < close_index; j++){
                            o.data.push(r.stringdata[j]);
                        }
                        r.stringdata.splice(open_index,(close_index+1)-open_index);
                        //console.log(o);
                        r.optdatastructs.push(o);
                    }
                }
            } else {
                // console.log(opentelopt);
                // console.log(closetelopt);
            }
            return r;
        }
        public getsequence(one:enums.telopt,two:enums.telopt) : number[] {
            var $this = this;
            var a = [];
            var sequencetwo = []; 
            for (var i = 0; i < $this.chunk.length; i++) {
                if(i+1 < $this.chunk.length){
                    sequencetwo = [
                        $this.chunk[i]
                        ,$this.chunk[i+1]
                    ];                    
                    if (sequencetwo[0] == one && sequencetwo[1] == two){
                        a.push(i);
                    }
                }
            }
            return a;
        }
        public getsequencearray(one:enums.telopt,two:enums.telopt[]) : number[] {
            var $this = this;
            var a = [];
            two.forEach(function(v,i,arr){
                var b = $this.getsequence(one,v);
                b.forEach(function(v,i,arr2){
                    a.push(v);
                });
            });
            return a;
        }
    }
}