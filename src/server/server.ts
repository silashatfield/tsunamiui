namespace tsu {
  export class server {
    public express;
    public webapp;
    public path;
    public webserver;
    public socketio;
    public port = 8004;

    constructor(){
      var $this = this;
      //variables setup
      this.express = require('express');
      this.webapp = this.express();
      this.path = require('path');
      this.webserver = require('http').Server(this.webapp);
      this.socketio = require('socket.io')(this.webserver);
      var Net = require('net');

      //start the server
      this.webapp.use('/', this.express.static(__dirname + '/../'));
      this. webserver.listen(this.port, '0.0.0.0', function () {
        console.log('Listening on ' + $this.webserver.address().port);
      });  

      //Handle a SOCKETIO error
      this.socketio.on('error', function ( socket ) {
        socket.tcpclient.destroy();
      });

      //This handles a new connection and setting it up
      this.socketio.on('connection', function (socket) {

          //set a new client on the socket object
          if(socket.tcpclient) socket.tcpclient.destroy();

          //setup new variables for this socket
          socket.tcpclient = new Net.Socket();
          socket.tcpclient.setNoDelay(true);
          socket.loggedin = false;    
          socket.tcpclient.socket = socket;
          socket.SP = new parseengine();
          socket.SP.chunk = [];
          socket.SP.socket = socket;       
          socket.queue = [];  
          socket.relog = false;        
          
          var options = {
            hostname: 'tsunami.thebigwave.net',
            port: 23
            //localAddress : '192.168.0.100',
            //localPort: portstart
          };    

          //socket handle incoming client data
          socket.tcpclient.on('data', function(chunk) {
              //console.log(chunk.toString() + "\n\n");
              try{
                  socket.queue.push( chunk );                           
              }catch(err){
                  console.log(err);
              }        
          });
          
          function connect(){
            //make a new connection
            socket.tcpclient.connect(23, 'thebigwave.net', function() {  
              //console.log(console.log(options));      
              console.log('A new TCP connection established with the server.');
            });
          }

          socket.logout = function (){
            socket.loggedin = false;
            socket.relog = false;
            //connect();
            try{
              socket.emit("logout");
            } catch(e){}
          }
          
          //close the tcp socket
          socket.tcpclient.on('close', function(e) {
            socket.loggedin = false;
            console.log('Requested an end to the TCP connection');
            console.log(e);
            socket.logout();
            //todo EMIT NOT READY SIGNAL
            //connect();
          });
          
          //handle client tcp error
          socket.tcpclient.on('error', function(e) {
            console.log(e);
            socket.logout();
          });

          connect();

          //queue for process stuff for this socket
          socket.processqueue = function(){
            socket.queue.forEach(function(v,i,a){     
              v.forEach(function(c){
                socket.SP.chunk.push(c); //we convert the chunk into our byte array, so we can splice it up
              });
              socket.SP.flush(socket);
            });
            socket.queue = [];
            setTimeout(function(){
              socket.processqueue();
            },1);
          }
          socket.processqueue();    

          //get user input
          socket.on('input', function (input) {
              // if(data == "gmcp1") {
              //future alias commands
              // }
              var data = input.trim();
              if(data === "logout"){
                socket.logout();
              }
              data += "\n";
              
              //console.log(data);
              //socket.emit("input",data);
              socket.tcpclient.write(data);

          });
          socket.on('test', function () {
              console.log('Client just requested test.');
              this.socketio.emit('test', "test");
          });

          //todo EMIT READY SIGNAL
      });
    }
  }
}  

var tsunamiui_server = new tsu.server(); //this starts the server
