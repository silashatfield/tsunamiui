namespace tsu {
    export namespace enums {
        export class telopt {
            public static ENVIRON = 36;
            public static GMCP = 201;
            public static SE = 240;
            public static NOP = 241;
            public static DATAMARK = 242;
            public static BREAK = 243;
            public static INTERRUPTPROCESS = 244;
            public static ABORTOUTPUT = 245;
            public static AREYOUTHERE = 246;
            public static ERASECHARACTER = 247;
            public static ERASELINE = 248;
            public static GOAHEAD = 249;
            public static SB = 250;
            public static WILL = 251;
            public static WONT = 252;
            public static DO = 253;
            public static DONT = 254;
            public static IAC = 255;     
        }
        export class environ {
            public static IS = 0;
            public static SEND = 1;
            public static INFO = 2;

            public static VAR = 0;
            public static VALUE = 1;
            public static ESC = 2;
            public static USERVAR = 3;

            public static IP = 5;              
        }
    }
}