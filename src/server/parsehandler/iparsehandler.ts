namespace tsu {
    export namespace parsehandler {
        export interface iparsehandler {
            process(socket : any, input : string) : string;
        }
    }
}