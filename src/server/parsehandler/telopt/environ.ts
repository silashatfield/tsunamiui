namespace tsu {
    export namespace parsehandler {
        export class environ implements itelopthandler {
            sendip(socket: any): void {
                if(socket.isusingenviron && socket.isusingenviron == 1){
                    var ip = socket.request.headers['x-real-ip'] || socket.request.connection.remoteAddress;
                    socket.tcpclient.write( (<any>Buffer).from([enums.telopt.IAC, enums.telopt.SB, enums.telopt.ENVIRON], "binary") );
                    socket.tcpclient.write(`${enums.environ.IS} ${enums.environ.VAR} ${enums.environ.IP} ${enums.environ.VALUE} ${ip}`);
                    socket.tcpclient.write( (<any>Buffer).from([enums.telopt.IAC, enums.telopt.SE], "binary") );
                }
            }
            processdata(socket: any, input: any): void {
                if(input.type != enums.telopt.ENVIRON) return;

                //console.log("Getting Send Request");
                console.log(input);

                //IAC SB ENVIRON SEND VAR IP IAC SE
                try{
                    if(input.data[0] == enums.environ.SEND){
                        for(var i=1; i<input.data.length; i++){
                            if(input.data[i] == enums.environ.VAR){
                                if(i+1 >= input.data.length) continue;
                                //todo this might need to change in the future to handle string variable names like "IP" instead of custom enum byte values
                                switch(input.data[i+1]){
                                    case enums.environ.IP:
                                        //console.log(socket.request.headers);
                                        //console.log(socket.request.connection);
                                        this.sendip(socket);
                                        break;
                                }
                            }
                        }
                    }
                } catch(e){
                    console.log(e);
                }
                
            }
            processnegotiation(socket: any, input: any): void {
                if(input.type != enums.telopt.ENVIRON) return;

                if(input.action == enums.telopt.DO){
                    console.log("Sending WILL ENVIRON for this socket");
                    //IAC WILL ENVIRON, we do ENVIRON
                    socket.isusingenviron = 1;
                    socket.tcpclient.write((<any>Buffer).from([enums.telopt.IAC, enums.telopt.WILL, enums.telopt.ENVIRON], "binary")); 
                    //socket.tcpclient.write((<any>Buffer).from([enums.telopt.IAC, enums.telopt.SB, enums.telopt.ENVIRON, enums.environ.IS, enums.environ.VAR].concat(helpers.toArrayBuffer((<any>Buffer).from("\"IP\"","binary"))).concat([enums.environ.VALUE]).concat(helpers.toArrayBuffer((<any>Buffer).from("\""+socket.handshake.address+"\"","binary"))).concat([enums.telopt.IAC, enums.telopt.SE]), "binary"));   
                }
            }
        }
    }
}