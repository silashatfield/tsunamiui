namespace tsu {
    export namespace parsehandler {
        export class gmcp implements itelopthandler {
            processdata(socket: any, input: any): void {
                if(input.type != enums.telopt.GMCP) return;

                //The server is sending us a JSON object of data to handle
                var jsonbytes = [];
                var fc = new filter_color();
                var json = fc.process( socket, (<any>Buffer).from(input.data).toString() );
                //console.log(json);
                if(json.length > 0){      
                    json = helpers.cleanString(json).trim();
                    var name = json.substr(0,json.indexOf('{')-1).trim();
                    var j = json.substr(json.indexOf('{')-1).substr(0, json.lastIndexOf("}")).trim();   
                    try {
                        JSON.parse(j);
                        socket.emit(name, j);   
                    } catch (e) { 
                        console.log("ERROR: " + name);            
                    }
                }
            }
            processnegotiation(socket: any, input: any): void {
                if(input.type != enums.telopt.GMCP) return;
                
                if(input.action == enums.telopt.WILL){
                    //IAC DO GMCP, we do GMCP
                    console.log("Sending DO GMCP for this socket");                    
                    socket.tcpclient.write((<any>Buffer).from([enums.telopt.IAC, enums.telopt.SB, enums.telopt.GMCP].concat(helpers.toArrayBuffer((<any>Buffer).from("enable gmcp","binary"))).concat([enums.telopt.IAC, enums.telopt.SE]), "binary"));                   
                }
            }
        }
    }
}