namespace tsu {
    export namespace parsehandler {
        export interface itelopthandler {
            processdata(socket : any, input : object) : void;
            processnegotiation(socket : any, input : object) : void;
        }
    }
}