namespace tsu {
    export namespace parsehandler {
        export class vote implements iparsehandler {
            process(socket: any, input : string) : string {
                
                if(input.includes('http://bit.ly/')) socket.emit('vote');
                
                return input;
            }
        }
    }
}