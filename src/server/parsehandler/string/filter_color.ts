namespace tsu {
    export namespace parsehandler {
        export class filter_color implements iparsehandler {
            process(socket : any, input : string) : string {
                return input.replace(/\[color=.*?\]/g,"").replace(/\[\/color\]/g,""); ;
            }
        }
    }
}