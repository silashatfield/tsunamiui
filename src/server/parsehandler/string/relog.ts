namespace tsu {
    export namespace parsehandler {
        export class relog implements iparsehandler {
            process(socket : any, input : string) : string {
                
                if(input.includes("Throw the other copy out? [y/n]")){
                    socket.relog = true;                    
                }

                return input;
            }
        }
    }
}