namespace tsu {
    export namespace parsehandler {
        export class prompt implements iparsehandler {
            process(socket : any, input : string) : string {
                var lines = input.match(/[^\r\n]+/g);
                var isprompt = false;
                if(lines)
                    lines.forEach(function(l){
                        if(l.trim() == ">"){
                            isprompt = true;
                            socket.emit("onprompt",input);
                        }
                    });  

                if(!isprompt && input.trim().length > 0) socket.emit("ondata", input);

                return input;
            }
        }
    }
}