namespace tsu {
    export namespace enums {
        export class events {
            public static readonly vote : string = 'vote';
            public static readonly tab_move : string = 'tabmove';
            public static readonly area : string = 'area';
            public static readonly room : string = 'room';
            public static readonly logout : string = 'logout';
            public static readonly loggedin : string = 'loggedin';
            public static readonly char_items : string = 'char.items';
            public static readonly char_vitals : string = 'char.vitals';
            public static readonly char_status : string = 'char.status';

            public static readonly onprompt : string = 'onprompt';
            public static readonly ondata : string = 'ondata';
            public static readonly oncolor : string = 'oncolor';

            public static readonly afterload_socket : string = 'afterload_socket';
            public static readonly after_login : string = 'afterload_login';
            public static readonly after_logout : string = 'afterload_login';
            public static readonly afterload_terminal : string = 'afterload_terminal';
            public static readonly afterload_database : string = 'afterload_database';
            public static readonly afterload_elementengine : string = 'afterload_elementengine';
        }
    }
}