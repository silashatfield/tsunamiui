namespace tsu {
    export namespace templates {
        export class defaulttemplate extends atemplate {
            constructor(){
                super();
                this.styles = `
                
                    *{
                        box-sizing: border-box;
                        -webkit-touch-callout: none; /* iOS Safari */
                        -webkit-user-select: none; /* Safari */
                        -khtml-user-select: none; /* Konqueror HTML */
                        -moz-user-select: none; /* Firefox */
                        -ms-user-select: none; /* Internet Explorer/Edge */
                        user-select: none; /* Non-prefixed version, currently
                        supported by Chrome and Opera */
                    }
                    body, html, #center{
                        height: 100%;
                    }
                    .input {
                        width: 100%;
                        height: 1.5em;
                        border-radius: 10px;  
                    }
                    #terminal{
                        margin: 5px;
                        width: 100%;
                    }
                    html, body {
                        font-size: 12px;
                        background:	#3d4454;
                        width:		100%;
                        height:		100%;					
                        padding:	0;
                        margin:		0;
                        overflow:	auto; 
                    }
                    .statbar{
                        width: 100%;        
                        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25) inset;
                    }
                    .tablestat{
                        width: 3%;
                        color: #ffffff;
                        font-weight: bold;
                    }
                    #spbar {
                        height: 4px;  
                    }
                    #spbar .progress-fill {
                        background:blue;
                    }
                    #hpbar {
                        height: 4px;            
                    }
                    #hpbar .progress-fill {
                        background:green;
                    }
                    #expbar .progress-fill {
                        background:cyan;
                    }
                    .progress-bar {
                        background:#ccc;
                        padding:10px 0;
                        border: 1px solid white;
                        border-radius: 10px;
                        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25) inset;
                        width:100%;
                        text-align:center;
                        position:relative;            
                    }
                    .progress-fill {
                        border-radius: 10px;            
                        color:#fff;
                        width:100%;
                        padding:10px 0;
                        position:absolute;
                        left:0;
                        top:0;
                        overflow:hidden;
                    }
                    #logo {
                        width: 50%;
                    }
                    #leftcol {
                        background-image: url('/images/vborder.png');
                        background-position: right;
                        background-repeat: repeat-y;
                        height:100vh;
                    }
                    #leftcoltop {
                        position: relative;
                    }
                    #leftcolmiddle {
                        height: 33.33333vh;
                        padding-left: 5px;
                    }
                    #leftcolbottom {                        
                        margin-top: 5px;
                        height: 33.33333vh;
                        padding-left: 5px;
                    }
                    #midcol {
                    
                    }
                    #rightcol {
                        background-image: url('/images/vborder.png');
                        background-position: left;
                        background-repeat: repeat-y;
                        height:100vh;
                    }
                    #rightcoltop {
                        height: 48vh;
                        padding-right: 5px;
                        margin-top: 5px;
                        justify-content: flex-end;
                    }
                    #rightcolbottom {
                        margin-top: 5px;
                        height: 48vh;
                        padding-right: 5px; 
                        justify-content: flex-end;   
                    }
                    #wave {
                        position: absolute;
                        max-height: 100%;
                    }
                    .tabs{
                        width: 70%;
                        background: rgba(0, 0, 0, 0.25);
                        border: none;
                        max-height: 100% !important;    
                        font-size: 8px;                    
                    }
                    #inventory, #equipment, #score, #realm, #area, #objects, #room{
                        max-height: 85% !important;
                        overflow-y: auto;
                        overflow-x: hidden;
                    }
                    #inventory_info{
                        margin-bottom: 5px;
                        color: red;
                        font-weight: bold;
                    }  
                    #inventory_search_input{
                        margin-bottom: 5px;
                    }       
                    .hide {
                        display: none;
                    }           
                    .inventory_item, .equipment_item, .object_item, .room_item{
                        text-overflow: ellipsis; /* will make [...] at the end */
                        width: 90px; /* change to your preferences */
                        white-space: nowrap; /* paragraph to one line */
                        overflow:hidden; /* older browsers */
                        cursor: pointer;
                        margin-bottom: 5px;
                    }
                    .equipment_item label{
                        min-width: 67px;                        
                    }
                    .equipment_slot{
                        color: red;
                        font-weight: bold;
                    }
                    .inventory_item_action, .equipment_item_action, .object_item_action, .room_item_action{
                        padding-left: 10px;                        
                    }
                    .inventory_item_action:hover, .equipment_item_action:hover, .object_item_action:hover, .room_item_action:hover{
                        background-color: #999;                       
                    }

                    #equipment_info {
                        margin-bottom: 5px;
                        color: red;
                        font-weight: bold;
                    }

                    #score_info {
                    }
                    #score_info label {
                        min-width: 75px;
                        font-weight: bold;
                        overflow: hidden;
                    }                    

                    /*DIALOG*/
                    .ui-dialog-content.ui-widget-content{
                        color: black;
                    }
                    .ui-widget-overlay.ui-front{
                        opacity: 0.15;
                    }

                    .center {
                        text-align: center;
                    }
                    .red {
                        color: red;
                    }
                    .blue {
                        color: cyan;
                    }
                    .green {
                        color: green;
                    }
                    .yellow {
                        color: yellow;
                    }
                    .orange {
                        color: orange;
                    }
                    .white {
                        color: white;
                    }                    

                    .collapsed {
                        display: none;
                    }
                    .expanded {
                        display: block;
                    }
                    .ui-widget-content{
                        color: white;
                    }
                    .ui-tabs-nav, .ui-tabs-anchor {
                        background: none;	
                    }
                    
                    .ui-tabs .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor, .ui-tabs .ui-tabs-nav li.ui-state-disabled .ui-tabs-anchor, .ui-tabs .ui-tabs-nav li.ui-tabs-loading .ui-tabs-anchor{
                        cursor: pointer;
                    }
                    
                    .ui-tabs .ui-tabs-nav .ui-tabs-anchor {
                        padding: 0px;
                    }
                    .ui-widget-header {
                        border: none;
                    }

                    #bodycontainer, body, html {
                        min-height: 100vh;
                        height: 100vh;
                        max-height: 100vh;
                        overflow-y: hidden;
                    }

                    .hidden {
                        display: none;
                    }

                    #keyboardcontainer {
                        min-width: 115%;
                        margin-left: -8%;
                        min-height: 180px;
                    }
                    .simple-keyboard{
                        min-height: 180px !important;
                    }
                    
                    /* Small devices (landscape phones, 576px and up) */
                    @media (min-width: 576px) { 
                    
                    }
                    
                    /* Medium devices (tablets, 768px and up) */
                    @media (min-width: 768px) { 
                        .tabs{ 
                            font-size: 12px;                    
                        }
                        .inventory_item, .equipment_item{
                            width: 150px;
                        }
                    }
                    
                    /* Large devices (desktops, 992px and up) */
                    @media (min-width: 992px) {  }
                    
                    /* Extra large devices (large desktops, 1200px and up) */
                    @media (min-width: 1200px) {  }


                    @media screen and (min-width: 320px) and (max-width: 767px) and (orientation: portrait) {
                       /* html {
                          transform: rotate(-90deg);
                          transform-origin: left top;
                          width: 100vh;
                          height: 100vw;
                          overflow-x: hidden;
                          position: absolute;
                          top: 100%;
                          left: 0;
                        } */
                      }

                `;
                this.template = `
                    <div id="bodycontainer" class="container-fluid d-flex flex-column">
                        <div class="row">
                            <div id="leftcol" class="col col-lg-2">                    
                                <div id="leftcoltop" class="row">
                                    <img id="logo" src="/images/top_left.png" />
                                </div>       
                                <div id="leftcolmiddle" class="row">
                                    <div id="tabs1" class="tabs">
                                        <ul>
                                            <li><a href="#inventory">Inventory</a></li>
                                            <li><a href="#equipment">Equipment</a></li>
                                            <li><a href="#score">Score</a></li>
                                            <li><a href="#realm">Realm</a></li>
                                            <li><a href="#money">Money</a></li>
                                            <li><a href="#objects">Objects</a></li>
                                            <li><a href="#room">Room</a></li>
                                        </ul>
                                        <div id="inventory">
                                            <div id="inventory_search"></div>
                                            <div id="inventory_list"></div>
                                        </div>
                                        <div id="equipment"></div>
                                        <div id="score"></div>
                                        <div id="realm">Move To Load...</div>
                                        <div id="money"></div>
                                        <div id="objects">
                                            <div id="objects_list"></div>
                                        </div>
                                        <div id="room">
                                            <div id="room_list"></div>
                                        </div>
                                    </div>
                                </div>  
                                <div id="leftcolbottom" class="row">
                                    <div id="tabs2" class="tabs">
                                        <ul>                                            
                                        </ul>                                        
                                    </div>
                                </div>               
                            </div>
                            <div id="midcol" class="col col-lg-8">
                                <div class="row">
                                    <div id="terminalwrapper">
                                        <div id="terminal"></div>    
                                        <input type="text" id="userinput" class="input keyboardInput" value="" />                                        
                                        <div id="mapcontainer">
                                        </div>
                                    </div>
                                </div>   
                                <div class="row">
                                    <div id="keyboardcontainer" class="col-12 hidden">

                                    </div>
                                    <div id="hudcontainer" class="col-12">
                                        <table style="width: 100%; margin-top: 0px; font-size: 12px; text-align: right;">
                                            <tr>
                                                <td style="vertical-align: bottom;">
                                                    &nbsp;
                                                </td>
                                                <td style="vertical-align: bottom;">
                                                    <div class="container-fluid">
                                                        <div class="row" id="simpleactions">

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3" style="text-align:left; padding: 0;">
                                                                <img id="gold_img" src="/images/gold.png" /> <span id="gold"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td colspan="2" id="directioncontainer"></td>
                                            </tr>
                                            <tr>
                                                <td class="tablestat">EXP</td>
                                                <td colspan="3" class="tablebar">
                                                    <div id="expbar" class="progress-bar">
                                                        <div class="progress-fill"></div>                                                
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tablestat">HP</td>
                                                <td class="tablebar" style="width:45%;">
                                                    <div id="hpbar" class="progress-bar">
                                                        <div class="progress-fill"></div>                                                
                                                    </div>
                                                </td>
                                                <td class="tablestat">SP</td>
                                                <td class="tablebar">
                                                    <div id="spbar" class="progress-bar">
                                                        <div class="progress-fill"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="aliasbuttons"></td>
                                                <td colspan="3" id="aliases"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>    
                            </div>
                            <div id="rightcol" class="col col-lg-2">
                                <div id="rightcoltop" class="row">
                                    <div id="tabs3" class="tabs">
                                        <ul>
                                        </ul>
                                    </div>                                              
                                </div>        
                                <div id="rightcolbottom" class="row">
                                    <div id="tabs4" class="tabs">
                                        <ul>
                                        </ul>
                                    </div>                                              
                                </div>               
                            </div>       
                        </div>             
                    </div>                
                `;
            }
        }
    }
}