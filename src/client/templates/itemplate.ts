namespace tsu {
    export namespace templates {
        export interface itemplate {
            events : tsu.elements.ievent[];
            template : string;
            styles : string;
            load() : void;
        }
    }
}