namespace tsu {
    export namespace elements {
        export interface ielement {
            parentid : string; //only needed if there is a target container for this
            elementid : string;
            events : ievent[];
            template : string;
            styles : string;
            donotadd : boolean;
            on(event:enums.events, data:object) : void;
            clean() : void;            
            after_load() : void;
        }
    }
}