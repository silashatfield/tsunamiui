namespace tsu {
    export namespace elements {
        export interface ievent {
            eventtype : enums.events;
            func : Function;
        }
    }
}