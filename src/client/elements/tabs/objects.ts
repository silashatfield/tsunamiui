namespace tsu {
    export namespace elements {
        export class objects_tab extends atab {
            donotadd : boolean = true;
            elementid = '#objects';
            expandtracker : object = {};
            clean(){
                var $this = this; 
                $($this.elementid + '_list').empty();
            }
            constructor(){
                super();
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.room
                    ,func:function(obj){
                        var s = `
                        
                        `;
                        //console.log(obj);

                        var r = {};

                        for(var k in obj.objects){
                            var short = k;
                            if( !r.hasOwnProperty(short) ) {
                                r[short] = k;
                            }
                        }

                        for(var k in $this.expandtracker){
                            var found = false;
                            for(var k2 in r){
                                if(k2 == k) found = true;
                            }
                            if(!found) delete $this.expandtracker[k];
                        }

                        s += '<div class="center red">Objects</div>';

                        //living
                        for(var k in r){
                            var expanded = $this.expandtracker.hasOwnProperty(k);
                            var item = r[k];    
                            var alt = k;                     
                            s += `
                                <div class="object_item" title="${alt}" alt="${alt}">
                                    ${alt}
                                    <span class="${expanded ? 'expanded' : 'collapsed'}">
                                        <div class="object_item_action" data-action="look">LOOK</div>
                                    </span>
                                </div>
                            `;
                        } 

                        $($this.elementid + ' #objects_list').empty().append(s);
                    }
                });   
                $('body').on('click','.object_item', function(){
                    var s = $(this).attr('title');
                    var $target = $(this).find('span');
                    var expanded = $target.hasClass('expanded');
                    if(expanded) {
                        $target.removeClass('expanded');
                        $target.addClass('collapsed');
                        delete $this.expandtracker[s];
                    } else {
                        $target.removeClass('collapsed');
                        $target.addClass('expanded');
                        $this.expandtracker[s] = true;
                    }
                });
                $('body').on('click','.object_item_action',function(){
                    var $parent = $(this).closest('.object_item');
                    var s = $parent.attr('title');
                    var action = $(this).data('action');
                    $this.socket.emit('input',action + ' ' + s.toLowerCase().replace(/ \(.*?\)/,''));
                });
            }
        }    
    }
}