namespace tsu {
    export namespace elements {
        export class score_tab extends atab {
            donotadd : boolean = true;
            elementid = '#score';
            expandtracker : object = {};
            clean(){
                var $this = this; 
                $($this.elementid).empty();
            }
            constructor(){
                super();
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.char_status
                    ,func:function(obj){
                        //console.log(obj);

                        var s = `
                            <div id="score_info">
                                <div class="center red">${obj.short}</div>
                                <div><div class="blue">Level</div> ${obj.levelfract ? `${obj.levelfract}` : `${obj.level}`}</div>
                                ${ obj.exptolevel ? `<div><div class="blue">Exp to Level</div>${obj.exptolevel}</div>` : '' }
                                ${ obj.wizlevel ? `<div><div class="blue">Wiz Level</div>${obj.wizlevel}</div>` : '' }
                                <div><div class="blue">Class</div>${obj.class} (${obj.subclass})</div>
                                <div><div class="blue">Race</div>${obj.race}</div>
                                <div><div class="blue">Gender</div>${obj.gender}</div>
                            </div>
                        `;

                        $($this.elementid).empty().append(s);
                    }
                });
            }
        }    
    }
}