namespace tsu {
    export namespace elements {
        export class atab implements ielement {
            parentid: string;  
            elementid: string;
            events: ievent[] = [];
            template : string;
            styles : string;
            donotadd : boolean = false;
            db : Storage = null;
            socket : any = null;
            on(event: enums.events, data: object): void {

            }
            clean(): void {

            }
            after_load(): void {

            }
            move_tab(targ) : void {
                var $this = this;
                var target = $this.elementid.replace('#','');
                //debugger;
                var $_this = $(`[aria-controls=${target}]`);
                //console.log($_this, `[aria-controls=${target}]`);
                var $target  = $('#'+targ);
                var $ul = $target.find('ul');
                var $parent = $ul.parent();
                $ul.append( $_this );
                $parent.append( $('#'+target) );
                (<any>$(".tabs")).tabs("refresh");
                (<any>$target).tabs( "option", "active", -1 );
            }
            constructor() {
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.afterload_socket
                    ,func:function(socket){
                        $this.socket = socket;
                    }
                });
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.afterload_database
                    ,func:function(database){
                        $this.db = database;
                        var value = $this.db.getItem($this.elementid);
                        if(value)
                            $this.move_tab(value);
                    }
                });
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.tab_move
                    ,func:function(args) {
                        var thistype = $this.elementid.replace('#','');
                        var type = args[0];
                        var id = args[1];

                        if(thistype != type) return;

                        $this.db.setItem($this.elementid, id);
                    }
                });
            }
        }    
    }
}