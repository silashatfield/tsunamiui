namespace tsu {
    export namespace elements {
        export class room_tab extends atab {
            donotadd : boolean = true;
            elementid = '#room';
            expandtracker : object = {};
            clean(){
                var $this = this; 
                $($this.elementid + '_list').empty();
            }
            constructor(){
                super();
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.room
                    ,func:function(obj){
                        var s = `
                        
                        `;
                        //console.log(obj);

                        var r = {};

                        for(var k in obj.items){
                            var item = obj.items[k];
                            var alt = item.name && item.name.length > 0 ? item.name : item.short;
                            var short = alt;
                            if( !r.hasOwnProperty(short) ) {
                                r[short] = obj.items[k];
                                r[short].counter = 1;
                            } else {
                                r[short].counter++;
                            }
                        }

                        for(var k in $this.expandtracker){
                            var found = false;
                            for(var k2 in r){
                                if(k2 == k) found = true;
                            }
                            if(!found) delete $this.expandtracker[k];
                        }

                        s += '<div class="center red">Living</div>';

                        //living
                        for(var k in r){
                            var expanded = $this.expandtracker.hasOwnProperty(k);
                            var item = r[k];
                            if(item.isliving == 0) continue;        
                            var alt = item.name && item.name.length > 0 ? item.name : item.short;                     
                            s += `
                                <div class="room_item" title="${alt}" alt="${alt}" data-isliving="${item.isliving}">
                                    ${item.counter > 1 ? 'x'+item.counter : ''} ${item.short}
                                    <span class="${expanded ? 'expanded' : 'collapsed'}">
                                        <div class="room_item_action" data-action="look">LOOK</div>
                                    </span>
                                </div>
                            `;
                        } 
                        
                        s += '<hr><div class="center red">Items</div>';
                        
                        //items
                        for(var k in r){
                            var expanded = $this.expandtracker.hasOwnProperty(k);
                            var item = r[k];
                            if(item.isliving == 1) continue;  
                            var alt = item.name && item.name.length > 0 ? item.name : item.short;                          
                            s += `
                                <div class="room_item" title="${alt}" alt="${alt}" data-isliving="${item.isliving}">
                                    ${item.counter > 1 ? 'x'+item.counter : ''} ${item.short}
                                    <span class="${expanded ? 'expanded' : 'collapsed'}">
                                        <div class="room_item_action" data-action="get">GET</div>
                                        <div class="room_item_action" data-action="get all">GET ALL</div>
                                        <div class="room_item_action" data-action="look">LOOK</div>
                                    </span>
                                </div>
                            `;
                        }  

                        $($this.elementid + ' #room_list').empty().append(s);
                    }
                });   
                $('body').on('click','.room_item', function(){
                    var s = $(this).attr('title');
                    var $target = $(this).find('span');
                    var expanded = $target.hasClass('expanded');
                    if(expanded) {
                        $target.removeClass('expanded');
                        $target.addClass('collapsed');
                        delete $this.expandtracker[s];
                    } else {
                        $target.removeClass('collapsed');
                        $target.addClass('expanded');
                        $this.expandtracker[s] = true;
                    }
                });
                $('body').on('click','.room_item_action',function(){
                    var $parent = $(this).closest('.room_item');
                    var s = $parent.attr('title');
                    var action = $(this).data('action');
                    $this.socket.emit('input',action + ' ' + s.toLowerCase().replace(/ \(.*?\)/,''));
                });
            }
        }    
    }
}