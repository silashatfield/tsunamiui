namespace tsu {
    export namespace elements {
        export class realm_tab extends atab {
            donotadd : boolean = true;
            elementid = '#realm';
            expandtracker : object = {};
            clean(){
                var $this = this; 
                $($this.elementid).empty();
            }
            constructor(){
                super();
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.area
                    ,func:function(obj){
                        //console.log(obj);

                        /*
                            

                            > You are currently in: Mainland Town
                                Level Range: 1-19
                                You are currently on the Mainland.
                                In in the Mainland Town.
                                You have explored 45 of 46 rooms in this area. (97%)

                                ------------------
                                You are in a town, in a building.
                                This terrain has no effect on your combat abilities.
                                You cannot see the sky.

                                ------------------
                                This room accepts magical movement.

                                area: "Mainland Town"​
                                area_count: 46                            ​
                                area_explored: 45                            ​
                                area_exploredpercent: 97                            ​
                                areaid: "Mainland Town"                            ​
                                dimension: 0                            ​
                                domain: "Mainland"                            ​
                                gate: 1                            ​
                                gatedest: 1                            ​
                                id: "/room/town/se_corner"                            ​
                                island: 0                            ​
                                levels: "1-19"                            ​
                                locale: "You are currently Mainland"                            ​
                                location: "the Mainland Town"                            ​
                                magicmovement: 1                            ​
                                outdoors: 4                            ​
                                outdoors_phrase: "You are outdoors."                            ​
                                summon: 1                            ​
                                teleport: 1
                                terrain: Array [ "town" ]​
                                terrain_bonus: "This terrain has no effect on your combat abilities."                                ​
                                terrain_phrase: "in a town"                                ​
                                wizard: 0
                        */

                        var color = "red";
                        if(obj.terrain_adjustment > 0 && obj.terrain_adjustment < 10){
                            color = "yellow";
                        } else if (obj.terrain_adjustment >= 10){
                            color = "green";
                        } else if (obj.terrain_adjustment == 0){
                            color = "white";
                        }
                        var t = '';
                        obj.terrain.forEach(function(v,i,a){
                            t += (i > 0 ? ',' : '') + v
                        });
                        var s = `
                            <div id="realm_info">                                
                                <div class="center ${color}">-=${obj.area}=-</div>
                                <div><div class="blue">Level</div>${obj.levels}</div>
                                <div><div class="blue">Explored</div>${obj.area_explored}/${obj.area_count} (${obj.area_exploredpercent}%)</div>
                                ${ obj.wizard !== 0 ? `<div><div class="blue">Wizard</div>${obj.wizard}</div>` : '' }
                                <div><div class="blue">Outdoors</div>${obj.outdoors_phrase}</div>
                                ${t.length > 0 ? '<hr><div><div class="blue">Terrain</div>'+t+'</div>' : ''}
                                <hr>
                                <div><div class="blue">Magic Movement</div>${obj.magicmovement == 1 ? 'Yes' : 'No'}</div>
                                <div><div class="blue">Gate</div>${obj.gate == 1 ? 'Yes' : 'No'}</div>
                                <div><div class="blue">Summon</div>${obj.summon == 1 ? 'Yes' : 'No'}</div>
                                <div><div class="blue">Teleport</div>${obj.teleport == 1 ? 'Yes' : 'No'}</div>                                
                            </div>
                        `;

                        $($this.elementid).empty().append(s);
                    }
                });
            }
        }    
    }
}