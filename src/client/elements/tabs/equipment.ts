namespace tsu {
    export namespace elements {
        export class equipment_tab extends atab {
            donotadd : boolean = true;
            elementid = '#equipment';
            expandtracker : object = {};
            orderedslots = [
                "weapon"
                ,"alt_weapon"
                ,"shield"
                ,"armour"
                ,"helmet"
                ,"gloves"
                ,"boots"
                ,"cloak"
                ,"amulet"
                ,"ring"
                ,"ring 2"
                ,"ring 3"
                ,"ring 4"
                ,"eyewear"
                ,"other"
            ];
            clean(){
                var $this = this; 
                $($this.elementid).empty();
            }
            constructor(){
                super();
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.char_items
                    ,func:function(obj){    
                        var r = obj.slots;

                        for(var k in $this.expandtracker){
                            var found = false;
                            for(var k2 in r){
                                if(k2 == k) found =true;
                            }
                            if(!found) delete $this.expandtracker[k];
                        }

                        var s = `
                            <div id="equipment_info">
                                <div>AC Equip:     ${obj.acequipment}</div>
                                <div>AC Other:     ${obj.acother}</div>
                                <div>AC Toughness: ${obj.toughness}</div>
                            </div>
                        `;

                        $this.orderedslots.forEach(function(k){
                            if(r.hasOwnProperty(k)){
                                var expanded = $this.expandtracker.hasOwnProperty(k);
                                var item = r[k];
                                var isitem = typeof item === "object";
                                var na = item === "NA";
                                
                                if(na) s+= `<div class="equipment_item" data-slot="${k}" title="You can't wear this." alt="You can't wear this.">
                                    <div class="equipment_slot">${k}</div>
                                    You can't wear this.</div>`;
                                else if(!isitem) s+= `<div class="equipment_item" data-slot="${k}" title="${k}" alt="${k}">
                                    <div class="equipment_slot">${k}</div>
                                    nothing</div>`;
                                else {
                                    var button = `
                                        ${item.ac > 0 ? `
                                            <div class="equipment_item_action" data-action="${item.worn == 1 ? 'remove' : 'wear'}">${item.worn == 1 ? 'REMOVE' : 'WEAR'}</div>
                                        ` : ''}
                                        ${item.wc > 0 ? `
                                            <div class="equipment_item_action" data-action="${item.wielded == 1 ? 'unwield' : 'wield'}">${item.wielded == 1 ? 'UNWIELD' : 'WIELD'}</div> 
                                        ` : ''}                           
                                    `;
                                    s += `
                                        <div class="equipment_item" data-slot="${k}" title="${item.short}" alt="${item.short}">
                                            <div class="equipment_slot">${k} -  [${item.ac > 0 ? item.ac : item.wc}]</div>
                                            ${item.short}
                                            <span class="${expanded ? 'expanded' : 'collapsed'}">
                                                <div class="equipment_item_action" data-action="look">LOOK</div>
                                                <div class="equipment_item_action" data-action="drop">DROP</div>
                                                ${button}
                                            </span>
                                        </div>
                                    `;
                                }
                            }
                        });   

                        $($this.elementid).empty().append(s);
                    }
                });
                $('body').on('click','.equipment_item', function(){
                    var s = $(this).data('slot');
                    var $target = $(this).find('span');
                    if(!$target) return;

                    var expanded = $target.hasClass('expanded');
                    if(expanded) {
                        $target.removeClass('expanded');
                        $target.addClass('collapsed');
                        delete $this.expandtracker[s];
                    } else {
                        $target.removeClass('collapsed');
                        $target.addClass('expanded');
                        $this.expandtracker[s] = true;
                    }
                });
                $('body').on('click','.equipment_item_action',function(){
                    var $parent = $(this).closest('.equipment_item');
                    var s = $parent.attr('title');
                    var action = $(this).data('action');
                    $this.socket.emit('input',action + ' ' + s.toLowerCase().replace(/ \(.*?\)/,''));
                });
            }
        }    
    }
}