namespace tsu {
    export namespace elements {
        export class inventory_tab extends atab {
            donotadd : boolean = true;
            elementid = '#inventory';
            expandtracker : object = {};
            clean(){
                var $this = this; 
                $($this.elementid + '_list').empty();
            }
            constructor(){
                super();
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.char_items
                    ,func:function(obj){
                        var s = `
                            <div id="inventory_info">Carry: ${obj.carry} / ${obj.maxcarry} units.</div>
                        `;
                        //console.log(obj);

                        var r = {};

                        for(var k in obj.items){
                            var short = obj.items[k].short;
                            if( !r.hasOwnProperty(short) ) {
                                r[short] = obj.items[k];
                                r[short].counter = 1;
                            } else {
                                r[short].counter++;
                            }
                        }

                        for(var k in $this.expandtracker){
                            var found = false;
                            for(var k2 in r){
                                if(k2 == k) found =true;
                            }
                            if(!found) delete $this.expandtracker[k];
                        }

                        for(var k in r){

                            var expanded = $this.expandtracker.hasOwnProperty(k);
                            var item = r[k];
                            if(item.worn == 1 || item.wielded == 1) continue;
                            var button = `
                                ${item.ac > 0 ? `
                                    <div class="inventory_item_action" data-action="${item.worn == 1 ? 'remove' : 'wear'}">${item.worn == 1 ? 'REMOVE' : 'WEAR'}</div>
                                ` : ''}
                                ${item.wc > 0 ? `
                                    <div class="inventory_item_action" data-action="${item.wielded == 1 ? 'unwield' : 'wield'}">${item.wielded == 1 ? 'UNWIELD' : 'WIELD'}</div> 
                                ` : ''}                           
                            `;                            
                            s += `
                                <div class="inventory_item" title="${item.short}" alt="${item.short}" data-type="${item.type}">
                                    ${item.counter > 1 ? 'x'+item.counter : ''} ${ item.ac > 0 || item.wc > 0 ? `[${item.ac > 0 ? item.ac : item.wc}] ` : '' }${item.short}
                                    <span class="${expanded ? 'expanded' : 'collapsed'}">
                                        <div class="inventory_item_action" data-action="look">LOOK</div>
                                        <div class="inventory_item_action" data-action="drop">DROP</div>
                                        ${button}
                                        <div class="inventory_item_action" data-action="sell">SELL</div>
                                    </span>
                                </div>
                            `;
                        }                        

                        $($this.elementid + ' #inventory_list').empty().append(s);
                        $('#inventory_search_input').trigger('keypress');
                    }
                });
                $(document).ready(function(){
                    $('#inventory_search').append('<input class="input keyboardInput" type="text" id="inventory_search_input" placeholder="Search" />');
                });    
                $(document).on('keyup keypress keydown','#inventory_search_input', function(){
                    $this.do_search();
                });
                $('body').on('click','.inventory_item', function(){
                    var s = $(this).attr('title');
                    var $target = $(this).find('span');
                    var expanded = $target.hasClass('expanded');
                    if(expanded) {
                        $target.removeClass('expanded');
                        $target.addClass('collapsed');
                        delete $this.expandtracker[s];
                    } else {
                        $target.removeClass('collapsed');
                        $target.addClass('expanded');
                        $this.expandtracker[s] = true;
                    }
                });
                $('body').on('click','.inventory_item_action',function(){
                    var $parent = $(this).closest('.inventory_item');
                    var s = $parent.attr('title');
                    var action = $(this).data('action');
                    $this.socket.emit('input',action + ' ' + s.toLowerCase().replace(/ \(.*?\)/,''));
                });
            }
            do_search(){
                var $e = $('#inventory_search_input');
                var val = $e.val().toString();
                if(val.length == 0) {
                    $('.inventory_item').removeClass('hide');
                    return;
                }
                $('.inventory_item').each(function(){
                    var $t = $(this);
                    var hide = true;
                    var data = $t.data('type');
                    try{
                        if($t.attr('title').includes(val) || (data && data.includes(val))) hide = false;
                    } catch(e) {}
                    if(hide) $t.addClass('hide');
                });
            }
        }    
    }
}