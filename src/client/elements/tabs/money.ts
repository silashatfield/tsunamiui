namespace tsu {
    export namespace elements {
        export class money_tab extends atab {
            donotadd : boolean = true;
            elementid = '#money';
            expandtracker : object = {};
            clean(){
                var $this = this; 
                $($this.elementid).empty();
            }
            constructor(){
                super();
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.char_status
                    ,func:function(obj){

                        var s = `
                            <div id="money_info">
                                <div class="center red">-=Currency=-</div>
                                <div><div class="blue">Gold</div>${obj.gold}</div>
                                <div><div class="blue">Bank</div>${obj.gold_bank}</div>
                                ${obj.gold_treasury > 0 ? '<div><div class="blue">Treasury</div>'+obj.gold_treasury+'</div>' : ''}
                            </div>
                        `;

                        $($this.elementid).empty().append(s);
                    }
                });
            }
        }
    }
}