namespace tsu {
    export namespace elements {
        export namespace misc {
            export class look extends tsu.elements.aelement {
                public elementid = '#lookbutton';
                public parentid = '#simpleactions';
                clean(){
                    // var $this = this; 
                    // $($this.elementid).empty();
                }
                constructor(){
                    super();
                    var $this = this;      
                    $this.styles = `
                        ${$this.elementid} {
                            text-align:center; 
                            padding: 0;
                            cursor: pointer;
                            color: purple;
                            border: 1px solid white;
                            border-radius: 5px;
                            font-size: 10px;
                            margin-bottom: 5px;
                        }
                        ${$this.elementid}:hover {
                            border-color: red;
                        }
                        /* Medium devices (tablets, 768px and up) */
                        @media (min-width: 768px) { 
                            ${$this.elementid} {
                                font-size: 30px;
                            }
                        }
                    `;
                    $this.template = `
                        <div id="${$this.elementid.replace("#","")}" class="col-1" data-action="look" title="Look" alt="Look">
                            <i class="fa fa-eye"></i>
                        </div>
                    `;   
                    $('body').on('click',`${$this.elementid}`,function(){
                        var $t = $(this);
                        if(!loggedin) return;
                        var action = $t.data('action');
                        $this.socket.emit('input',action);
                    });
                }
            }
        }    
    }
}