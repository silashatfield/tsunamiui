declare function toggleFullScreen();
namespace tsu {
    export namespace elements {
        export namespace misc {
            export class keyboard extends tsu.elements.aelement {
                public elementid = '#keyboard';
                public parentid = '#keyboardcontainer';
                public input;
                constructor(){
                    super();
                    var $this = this;
                    $this.styles = `
                        ${this.elementid}:hover {    
                            
                        }
                    `;
                    $this.template = `
                        <div class="simple-keyboard"></div> 
                    `;
                    
                    $this.events.push(<elements.ievent>{
                        eventtype: tsu.enums.events.afterload_elementengine
                        ,func:function(obj) {

                            let Keyboard = (<any>window).SimpleKeyboard.default;
                            let keyboard = new Keyboard({
                                onChange: input => onChange(input),
                                onKeyPress: button => onKeyPress(button),
                                mergeDisplay: true,
                                layoutName: "default",
                                useMouseEvents: true,
                                useTouchEvents: false,
                                layout: {
                                    default: [
                                        "q w e r t y u i o p",
                                        "a s d f g h j k l",
                                        "{shift} z x c v b n m {backspace}",
                                        "{numbers} {space} {ent}"
                                    ],
                                    shift: [
                                        "Q W E R T Y U I O P",
                                        "A S D F G H J K L",
                                        "{shift} Z X C V B N M {backspace}",
                                        "{numbers} {space} {ent}"
                                    ],
                                    numbers: ["1 2 3", "4 5 6", "7 8 9", "{abc} 0 {backspace}"]
                                },
                                display: {
                                    "{numbers}": "123",
                                    "{ent}": "return",
                                    "{escape}": "esc ⎋",
                                    "{tab}": "tab ⇥",
                                    "{backspace}": "⌫",
                                    "{capslock}": "caps lock ⇪",
                                    "{shift}": "⇧",
                                    "{controlleft}": "ctrl ⌃",
                                    "{controlright}": "ctrl ⌃",
                                    "{altleft}": "alt ⌥",
                                    "{altright}": "alt ⌥",
                                    "{metaleft}": "cmd ⌘",
                                    "{metaright}": "cmd ⌘",
                                    "{abc}": "ABC"
                                }
                            });

                            //$('body').off('pointerdown','.simple-keyboard');
                            $("body").on('focus','.keyboardInput',function(e){                              
                                onInputFocus( e );
                            });
                            $("body").on('input','.keyboardInput',function(e){                              
                                onInputChange( e );
                            });
                            
                            function onInputFocus(event) {
                                $this.input = `#${event.target.id}`;
                                //console.log($this.input);
                                keyboard.setOptions({
                                    inputName: event.target.id
                                });
                            }
                            
                            function onInputChange(event) {
                                keyboard.setInput(event.target.value, event.target.id);
                            }
                            
                            function onChange(input) {
                                //console.log("Input changed", input);
                                //console.log($this.input);
                                document.querySelector($this.input).value = input;
                            }
                            
                            function onKeyPress(button) {
                                //console.log("Button pressed", button);
                                if(!$this.input){
                                    $this.input = '#userinput';
                                }
                            
                                /**
                                 * Shift functionality
                                 */
                                if (button === "{lock}" || button === "{shift}") handleShiftButton();
                                if (button === "{numbers}" || button === "{abc}") handleNumbers();
                                if (button === "{ent}"){
                                    $($this.input).trigger(jQuery.Event('keypress', { charCode: 13 }));
                                    keyboard.setInput("", $this.input.replace('#',''));
                                    //$($this.input).trigger('focus');
                                    //$($this.input).select();
                                }
                            }
                            
                            function handleShiftButton() {
                                let currentLayout = keyboard.options.layoutName;
                                let shiftToggle = currentLayout === "default" ? "shift" : "default";
                            
                                keyboard.setOptions({
                                    layoutName: shiftToggle
                                });
                            }
                            
                            function handleNumbers() {
                                let currentLayout = keyboard.options.layoutName;
                                let numbersToggle = currentLayout !== "numbers" ? "numbers" : "default";

                                keyboard.setOptions({
                                    layoutName: numbersToggle
                                });
                            }


                        }
                    });
                }
            }
        }    
    }
}