namespace tsu {
    export namespace elements {
        export namespace misc {
            class alias {
                public name : string = "";
                public action : string = "";
                public icon : string = "";
                public color : string = "";
            }
            class fonticon {
                public icon : string = "";
                public name : string = "";
            }
            export class aliases extends tsu.elements.aelement {
                public elementid = '#aliases';
                public parentid = '#aliasbuttons';
                public aliases : alias[] = [];
                public icons : fonticon[] = [];
                public colors : string[] = [];
                public username : string = "";
                public deletemode = false;
                clean(){
                    var $this = this; 
                    $this.username = "";
                    $this.aliases = [];
                    $($this.elementid).empty();
                    try{ (<any>$('#addalias_dialog')).dialog("close"); } catch(e){}
                    $('#aliasactionbuttons').removeClass('hide');
                    $('#aliascancelbutton').addClass('hide');
                    $this.deletemode = false;
                }
                constructor(){
                    super();
                    var $this = this;
                    //Icon List
                    $this.icons.push({icon:"ra-muscle-up",name:"muscle"});
                    $this.icons.push({icon:"ra-aura",name:"aura"});
                    $this.icons.push({icon:"ra-player-dodge",name:"dodge"});
                    $this.icons.push({icon:"ra-heart-bottle",name:"heart potion"});
                    $this.icons.push({icon:"ra-vial",name:"vial"});
                    $this.icons.push({icon:"ra-flask ",name:"flask"});
                    $this.icons.push({icon:"ra-torch",name:"torch"});
                    $this.icons.push({icon:"ra-key",name:"key"});
                    $this.icons.push({icon:"ra-medical-pack",name:"medic"});
                    $this.icons.push({icon:"ra-bird-claw",name:"claw"});
                    $this.icons.push({icon:"ra-dragon",name:"dragon"});
                    $this.icons.push({icon:"ra-dice-five",name:"dice"});
                    $this.icons.push({icon:"ra-beer",name:"beer"});
                    $this.icons.push({icon:"ra-bone-bite",name:"skull"});
                    $this.icons.push({icon:"ra-snowflake",name:"snow"});
                    $this.icons.push({icon:"ra-fairy-wand",name:"wand"});
                    $this.icons.push({icon:"ra-flame-symbol",name:"flame"});
                    $this.icons.push({icon:"ra-dragon-breath",name:"fire breath"});
                    $this.icons.push({icon:"ra-broadhead-arrow",name:"arrow"});
                    $this.icons.push({icon:"ra-sword",name:"sword"});
                    $this.icons.push({icon:"ra-round-shield",name:"shield"});              
                    //Colors
                    $this.colors.push("red");
                    $this.colors.push("blue");
                    $this.colors.push("black");
                    $this.colors.push("orange");
                    $this.colors.push("green");
                    $this.colors.push("purple");
                    $this.colors.push("yellow");
                    $this.colors.push("pink");
                    
                    $this.styles = `
                        #addalias{
                            font-size: 25px; 
                            color: green; 
                            cursor: pointer;
                        }
                        #removealias, #cancelalias{
                            font-size: 25px; 
                            color: red; 
                            cursor: pointer;
                        }
                        #addalias_form option{
                            font-family: RPGAwesome;
                        }
                        .aliasbutton {
                            background-color: black;
                            border: 1px solid white;
                            border-radius: 5px;
                            box-shadow: 2px 2px 1px grey;
                            padding: 0;
                            margin-right: 5px;
                            cursor: pointer;
                        }
                        .aliasbutton:hover {
                            border-color: red;
                        }
                        .aliastitle{
                            border-radius: 5px 5px 0px 0px;
                            background-color: #ccc;
                            text-align: center;
                        }
                        .aliasicon {
                            text-align: center;
                        }                        
                    `;
                    var options = "";
                    $this.colors.forEach(function(c){
                        options += `<option>${c}</option>`;
                    });
                    var icons = "";
                    $this.icons.forEach(function(i){
                        icons += `<option value="${i.icon}">${i.name}</option>`;
                    });
                    $this.template = `
                        <div id="aliasactionbuttons">
                            <i id="addalias" class="fa fa-plus-square"></i>
                            <br/>
                            <i id="removealias" class="fa fa-minus-square"></i>
                        </div>
                        <div id="aliascancelbutton" class="hide">
                            <i id="cancelalias" class="fa fa-window-close"></i>
                        </div>

                        <div id="addalias_dialog" title="Add Action" style="display: none;">
                            <form id="addalias_form">
                                <div class="form-group">
                                    <label for="addalias_name">Name</label>
                                    <input type="text" class="form-control keyboardInput" id="addalias_name" placeholder="" maxlength="15">
                                </div>
                                <div class="form-group">
                                    <label for="addalias_action">Action</label>
                                    <input type="text" class="form-control keyboardInput" id="addalias_action" aria-describedby="actionHelp" placeholder="">
                                    <small id="actionHelp" class="form-text text-muted">The command sent</small>
                                </div>
                                <div class="form-group">
                                    <label for="addalias_color">Color</label>
                                    <select class="form-control" id="addalias_color">
                                        ${options}
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="addalias_icon">Icon</label>
                                    <select class="form-control" id="addalias_icon">
                                        ${icons}
                                    </select>
                                </div>
                            </form>
                        </div>
                    `;
                    $('body').on('click','#removealias',function(){
                        if(!loggedin) return;
                        $('#aliasactionbuttons').addClass('hide');
                        $('#aliascancelbutton').removeClass('hide');
                        $this.deletemode = true;
                    });
                    $('body').on('click','#cancelalias',function(){
                        if(!loggedin) return;
                        $('#aliasactionbuttons').removeClass('hide');
                        $('#aliascancelbutton').addClass('hide');
                        $this.deletemode = false;
                    });
                    $('body').on('click','#addalias',function(){
                        if(!loggedin) return;

                        (<any>$('#addalias_dialog')).dialog({
                            modal: helpers.isMobile() ? false : true
                            ,width: 200
                            ,height: helpers.isMobile() ? 160 : 375
                            ,position: { my: "center", at: "top" }
                            ,buttons: [
                                {
                                    text: "Save",
                                    //icon: "ui-icon-heart",
                                    click: function() {
                                        var alias = <alias>{
                                            name:$('#addalias_name').val(),
                                            action:$('#addalias_action').val(),
                                            color:$('#addalias_color').val(),
                                            icon:$('#addalias_icon').val()
                                        }
                                        
                                        $('#addalias_name').val('');
                                        $('#addalias_action').val('');
                                        $('#addalias_color').val('');
                                        $('#addalias_icon').val('');
                                        
                                        if(alias.name.length == 0 || alias.action.length == 0 || $this.username.length == 0) return;

                                        $this.aliases.push(alias);
                                        $this.db.setItem("aliases_" + $this.username,JSON.stringify($this.aliases));
                                        $this.add_aliases();
                                        
                                        (<any>$( '#addalias_dialog' )).dialog("close");
                                    }
                                },{
                                    text: "Exit",
                                    //icon: "ui-icon-heart",
                                    click: function() {
                                        (<any>$( '#addalias_dialog' )).dialog("close");
                                    }
                                }
                            ]
                        });
                        //$('.ui-widget-overlay').css('opacity','0.15');

                    });

                    $('body').on('click','.aliasbutton',function(){                        
                        var $t = $(this);
                        if(!$this.deletemode){
                            var action = $t.data('action');
                            $this.socket.emit('input',action);
                            return;
                        }

                        var index = $('.aliasbutton').index(this);
                        $this.aliases.splice(index,1);
                        $this.db.setItem("aliases_" + $this.username,JSON.stringify($this.aliases));
                        $this.add_aliases();
                    });
                    
                    $this.events.push(<elements.ievent>{
                        eventtype: enums.events.char_status
                        ,func:function(obj){
                            $this.username = obj.name;
                            var s = $this.db.getItem("aliases_" + obj.name);
                            if(!s) return;
                            $this.aliases = JSON.parse(s);
                            //console.log($this.aliases, s);
                            $this.add_aliases();
                        }
                    });
                }
                add_aliases(){
                    var $this = this;
                    var s = "<div class='container-fluid'><div class='row'>";
                    $this.aliases.forEach(function(v:alias){
                        s += `
                        <div class="col-1 aliasbutton" data-action="${v.action}">
                            <div class="aliastitle">${v.name}</div>
                            <div class="aliasicon" style="color:${v.color};"><i class="ra ra-2x ${v.icon}"></i></div>
                        </div>`;
                    });
                    s += "</div></div>";
                    $($this.elementid).empty().append(s);
                }
            }
        }    
    }
}