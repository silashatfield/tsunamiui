namespace tsu {
    export namespace elements {
        export namespace misc {
            export class settings extends tsu.elements.aelement {
                public elementid = '#settings';
                public parentid = '#leftcoltop';
                constructor(){
                    super();
                    var $this = this;
                    $this.styles = `
                        ${this.elementid} {
                            position: absolute;
                            top: 10px;
                            right: 65px;
                            z-index: 500;
                            height: 35px;
                            width: 35px;
                            font-size: 35px;
                            cursor: pointer;
                        }  
                        ${this.elementid}:hover {    
                            color: #ccc;
                        }
                    `;
                    $this.template = `
                        <span id="${this.elementid.replace('#','')}" class="ra ra-cog"></span>
                    `;
                }
            }
        }    
    }
}