namespace tsu {
    export namespace elements {
        export namespace misc {
            export class vote extends tsu.elements.aelement {
                public voteurl = 'http://bit.ly/TSUNAMIVOTE';
                public imageurl = '/images/vote.png';
                public elementid = '#vote';
                public parentid = 'body';
                constructor(){
                    super();
                    var $this = this;
                    $this.styles = `
                        ${this.elementid} {
                            display: none;
                            position: absolute;
                            z-index: 999999;
                            top: 50%;
                            left: 50%;
                            margin-left: -100px;
                            margin-top: -100px;
                            width: 200px;
                        }                      
                    `;
                    $this.template = `
                        <a href="${$this.voteurl}">
                            <img id="${$this.elementid.replace('#','')}" target="_blank"  src="${$this.imageurl}" />
                        </a>
                    `;
                    $this.events.push(<elements.ievent>{
                        eventtype: enums.events.vote
                        ,func:function(){
                            $($this.elementid).show();
                            setTimeout(function(){
                                $($this.elementid).hide();
                            },10000);
                        }
                    });
                }
            }
        }    
    }
}