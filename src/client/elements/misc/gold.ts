namespace tsu {
    export namespace elements {
        export namespace misc {
            export class gold extends tsu.elements.aelement {
                public elementid = '#gold';
                public donotadd = true;
                public socket = null;
                public gold = 0;
                clean(){
                    var $this = this; 
                    $($this.elementid).empty();
                }
                constructor(){
                    super();
                    var $this = this; 
                    $this.styles = `
                        #gold_img {
                            width: 10px;
                        }

                        #gold {
                            text-align: left;
                            font-size: 6px;
                            display: inline-block;
                        }
                        /* Medium devices (tablets, 768px and up) */
                        @media (min-width: 768px) { 
                            #gold_img {
                                width: 20px;
                            }    
                            #gold {
                                font-size: 15px;
                            }
                        }
                    `;                   
                    $this.events.push(<elements.ievent>{
                        eventtype: enums.events.char_status
                        ,func:function(obj){   
                            $this.gold = obj.gold;
                            $this.template = `
                                <div class="yellow">${$this.gold}</div>
                            `;
                            $($this.elementid).empty().append($this.template);
                        }   
                    });
                }
            }
        }    
    }
}