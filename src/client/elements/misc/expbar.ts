namespace tsu {
    export namespace elements {
        export namespace misc {
            export class expbar extends tsu.elements.aelement {
                public elementid = '#expbar';
                public donotadd = true;
                public socket = null;
                clean(){
                    var $this = this; 
                    $($this.elementid + ' .progress-fill').css("width", "100%");
                }
                constructor(){
                    super();
                    var $this = this;
                    $this.events.push(<elements.ievent>{
                        eventtype: enums.events.char_status
                        ,func:function(obj){    
                            $($this.elementid + ' .progress-fill').css("width", (obj.expfract*100)+"%");
                        }
                    });
                }
            }
        }    
    }
}