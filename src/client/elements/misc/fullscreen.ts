declare function toggleFullScreen();
namespace tsu {
    export namespace elements {
        export namespace misc {
            export class fullscreen extends tsu.elements.aelement {
                public elementid = '#fullscreen';
                public parentid = '#leftcoltop';
                constructor(){
                    super();
                    var $this = this;
                    $this.styles = `
                        ${this.elementid} {
                            position: absolute;
                            top: 45px;
                            right: 65px;
                            z-index: 500;
                            height: 35px;
                            width: 35px;
                            font-size: 35px;
                            cursor: pointer;
                        }  
                        ${this.elementid}:hover {    
                            color: #ccc;
                        }
                    `;
                    $this.template = `
                        <span id="${this.elementid.replace('#','')}" class="fa fa-expand"></span>
                    `;

                    $('body').on('click',`${this.elementid}`,function(){
                        toggleFullScreen();
                    });
                }
            }
        }    
    }
}