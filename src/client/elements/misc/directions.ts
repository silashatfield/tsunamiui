namespace tsu {
    export namespace elements {
        export namespace misc {
            export class directions extends tsu.elements.aelement {
                public elementid = '#directions';
                public parentid = '#directioncontainer';
                clean(){
                    $('#directions td').addClass('disabled').html("&nbsp;");
                }
                constructor(){
                    super();
                    var $this = this;
                    $this.styles = `
                        #directions {
                            font-size: 6px;
                            text-align: center;
                            background-image: url(/images/rose.png);
                            background-size: 40% 100%;
                            cursor: pointer;
                            color: white;
                            font-weight: bold;
                            background-repeat: no-repeat;
                        }
                        #directions td {                        
                            padding: 5px;
                            width: 25px; 
                            max-width: 25px;
                            text-overflow: ellipsis;
                            white-space: nowrap;
                            overflow:hidden;
                        }   
                        #directions td[data-container]{
                            text-align: left;   
                        }
                        #directions td:not(.disabled):hover {
                            background-color: rgba(255,0,0,.5);
                        }
                        /* Medium devices (tablets, 768px and up) */
                        @media (min-width: 768px) { 
                            #directions {
                                font-size: 12px;                    
                            }
                            #directions td {                        
                                padding: 5px;
                                width: 50px; 
                                max-width: 50px;                                       
                            }
                            #directions td[data-container]{
                                width: 75px;
                                max-width: 75px;
                                text-overflow: ellipsis;
                                white-space: nowrap;
                                overflow:hidden;
                            }
                        }
                    `;
                    $this.template = `
                        <table id="directions">
                            <tr>
                                <td class="disabled" data-action="nw">&nbsp;</td>
                                <td class="disabled" data-action="n">&nbsp;</td>
                                <td class="disabled" data-action="ne">&nbsp;</td>
                                <td class="disabled" data-container="1" data-action="">&nbsp;</td>
                                <td class="disabled" data-container="4" data-action="">&nbsp;</td>
                                <td class="disabled" data-container="7" data-action="">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="disabled" data-action="w">&nbsp;</td>
                                <td class="disabled" >&nbsp;</td>
                                <td class="disabled" data-action="e">&nbsp;</td>
                                <td class="disabled" data-container="2" data-action="">&nbsp;</td>
                                <td class="disabled" data-container="5" data-action="">&nbsp;</td>
                                <td class="disabled" data-container="8" data-action="">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="disabled" data-action="sw">&nbsp;</td>
                                <td class="disabled" data-action="s">&nbsp;</td>
                                <td class="disabled" data-action="se">&nbsp;</td>
                                <td class="disabled" data-container="3" data-action="">&nbsp;</td>
                                <td class="disabled" data-container="6" data-action="">&nbsp;</td>
                                <td class="disabled" data-container="9" data-action="">&nbsp;</td>
                            </tr>
                        </table>
                    `;
                    var shortmap = [
                        "nw"
                        ,"n"
                        ,"ne"
                        ,"w"
                        ,"e"
                        ,"sw"
                        ,"s"
                        ,"se"
                    ];
                    $('body').on('click','#directions td',function(){
                        var $t = $(this);
                        if($t.hasClass('disabled')) return;
                        var action = $t.data('action');
                        $this.socket.emit('input',action);
                    });
                    $this.events.push(<elements.ievent>{
                        eventtype: enums.events.room
                        ,func:function(obj){
                            $('#directions td').addClass('disabled').html("&nbsp;");
                            
                            //console.log(exits);

                            var containerindex = 0;
                            for(var e in obj.exits){
                                var needscontainer = !shortmap.includes(e);

                                if(!needscontainer){
                                    $(`[data-action=${e}]`).removeClass('disabled').html(e.toUpperCase());
                                } else {
                                    containerindex++;
                                    $(`[data-container="${containerindex}"]`).removeClass('disabled').html(e.toUpperCase());
                                    $(`[data-container="${containerindex}"]`).data("action",e);
                                }

                            };                            

                            //console.log(obj);
                        }
                    });
                }
            }
        }    
    }
}