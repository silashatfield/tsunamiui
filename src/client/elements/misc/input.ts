namespace tsu {
    export namespace elements {
        export namespace misc {
            export class input extends tsu.elements.aelement {
                public elementid = '#userinput';
                public donotadd = true;
                public socket = null;
                constructor(){
                    super();
                    var $this = this;
                    $this.styles = `

                    `;
                    $this.template = `

                    `;
                    $('body').on('keypress',`${this.elementid}`,function(e){
                        if(e.charCode == 13){
                            var val = $(this).val();
                            if(helpers.isMobile()) $(this).val('');
                            $(this).select();
                            $this.socket.emit('input',val);
                        }
                    });
                    $('body').on('click',`#terminal`,function(e){
                        $($this.elementid).focus();
                    });                    
                }
            }
        }    
    }
}