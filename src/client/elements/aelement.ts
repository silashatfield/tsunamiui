namespace tsu {
    export namespace elements {
        export class aelement implements ielement {
            parentid: string;  
            elementid: string;
            events: ievent[] = [];
            template : string;
            styles : string;
            donotadd : boolean = false;
            db : Storage = null;
            socket : any = null;
            on(event: enums.events, data: object): void {

            }
            clean(): void {

            }
            after_load(): void {

            }
            after_database():void{

            }
            constructor(){
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.afterload_socket
                    ,func:function(socket){
                        $this.socket = socket;
                    }
                });
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.afterload_database
                    ,func:function(database){
                        $this.db = database;
                        // var value = $this.db.getItem($this.elementid);
                        // if(value)
                        // $this.move_tab(value);
                        $this.after_database();    
                    }
                });
            }
        }    
    }
}