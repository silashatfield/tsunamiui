
declare var Terminal;
declare var fit;
namespace tsu {
    export namespace engines{
        export class terminal {
            public load() : any {
                Terminal.applyAddon(fit);
                var term = new Terminal();

                term.open(document.getElementById('terminal'));
                setTimeout(function(){ 
                    $(window).trigger('resize');
                }, 300);

                $( window ).resize(function() {
                    //scale term height
                    var rowsratio = 24/667;
                    var h = Math.floor((<number>$('#bodycontainer').innerHeight()+150) * rowsratio);
                    if(h < 10) h=10;
                    if(h > 50) h=50;
                    term.resize(term.cols,h);

                    //scale term font
                    var ratio = 6/667;                    
                    var s = Math.floor(window.innerWidth * ratio);
                    if(s < 6) s=6;
                    if(s > 14) s=14;
                    term.setOption("fontSize",s);            
                    term.fit(); 
                    console.log(term.rows)
                });
                
                // centerlayout.sizePane("north", "80%");
                // centerlayout.sizePane("south", "10%");
                return term;
            }
        }
    }
}