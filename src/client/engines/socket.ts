declare var io;
declare var loggedin;

namespace tsu {
    export namespace engines{
        export class socket extends tsu.elements.aelement{
            public donotadd : boolean = true;
            public socket = null;
            public term = null;
            public queue : String[] = [];
            public e_engine : engines.elements = null;
            constructor(){
                super();
                var $this = this;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.afterload_terminal
                    ,func:function(term){
                       $this.term = term;                       
                    }
                });
            }
            public process_queue() : void {
                var $this = this;
                if($this.term){                    
                    $this.queue.forEach(function(s){
                        $this.term.write(s);
                    });
                    $this.queue = [];
                }
                setTimeout(function(){
                    $this.process_queue();
                },1);
            }
            public load() : void {
                var $this = this;
                
                $this.socket = io.connect();
                $this.socket.on('input', function(data){
                    $this.queue.push(data);                    
                });
                $this.socket.on('data', function(data){
                    $this.queue.push(data);
                });
                $this.socket.on(enums.events.char_vitals, function(data){
                    var obj = JSON.parse(data);
                    //todo move to an element
                    //console.log(obj);
        
                    $('#hpbar .progress-fill').css("width", ((obj.hp/obj.max_hp)*100)+"%");
                    $('#spbar .progress-fill').css("width", ((obj.sp/obj.max_sp)*100)+"%");
                });
                $this.socket.on("char.status", function(data){
                    var obj = JSON.parse(data);
                    //console.log(obj);
                    $this.e_engine.emit(enums.events.char_status,obj);
                });
                $this.socket.on(enums.events.oncolor, function(data){
                    var obj = JSON.parse(data);
                    console.log("oncolor",obj);
                    $this.e_engine.emit(enums.events.oncolor, obj);
                });
                $this.socket.on(enums.events.onprompt, function(data){
                    // var obj = JSON.parse(data);
                    console.log("onprompt");
                    $this.e_engine.emit(enums.events.onprompt,null);
                });
                $this.socket.on(enums.events.ondata, function(data){
                    var obj = data;
                    console.log("ondata");
                    $this.e_engine.emit(enums.events.ondata,obj);
                });
                $this.socket.on(enums.events.char_items, function(data){
                    var obj = JSON.parse(data);
                    $this.e_engine.emit(enums.events.char_items,obj);
                });
                $this.socket.on(enums.events.area, function(data){
                    var obj = JSON.parse(data);                    
                    $this.e_engine.emit(enums.events.area,obj);
                });
                $this.socket.on(enums.events.room, function(data){
                    var obj = JSON.parse(data);
                    //console.log(obj);
                    $this.e_engine.emit(enums.events.room,obj);
                });
                $this.socket.on(enums.events.loggedin, function(){
                    //var obj = JSON.parse(data);
                    loggedin = true;   
                    $this.e_engine.emit(enums.events.after_login,null);
                });
                $this.socket.on(enums.events.logout, function(){
                    //var obj = JSON.parse(data);
                    loggedin = false;   
                    $this.e_engine.emit(enums.events.after_logout,null);
                    $this.e_engine.clean();
                });
                $this.socket.on(enums.events.vote, function(data){
                    $('#vote').show();
                    setTimeout(function(){
                        $('#vote').hide();
                    },10000);
                });    
                setTimeout(function(){
                    $this.process_queue();
                },1000);            
                return $this.socket;
            }
        }
    }
}