namespace tsu {
    export namespace engines{
        export class elements {
            public elements : tsu.elements.ielement[] = [];
            public add(elem : tsu.elements.ielement) : void {
                this.elements.push(elem);
            }
            public load() : void {
                var $this = this;
                for(var i = 0; i < $this.elements.length; i++){
                    var e = $this.elements[i];
                    if(e.styles) $('#corestyles').append(e.styles);

                    if(!e.donotadd)
                        if(e.parentid) {
                            $(e.parentid).append(e.template);
                            // if(e.parentid == '#leftcoltop'){
                            //     console.log($(e.parentid));
                            // }
                        } else $('body').append(e.template);
                }
            }
            public after_load() : void {
                var $this = this;
                for(var i = 0; i < $this.elements.length; i++){
                    var e = $this.elements[i];
                    e.after_load();
                }
            }
            public clean() : void {
                var $this = this;
                for(var i = 0; i < $this.elements.length; i++){
                    var e = $this.elements[i];
                    e.clean();
                }
            }
            public emit(eventtype:enums.events, data:object) : void {
                var $this = this;
                for(var i = 0; i < $this.elements.length; i++){
                    var e = $this.elements[i];
                    for(var j = 0; j < e.events.length; j++){
                        var event = e.events[j];
                        if(event.eventtype == eventtype)
                            event.func(data);
                    }
                }
            }
        }
    }
}