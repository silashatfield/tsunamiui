namespace tsu {
    export namespace engines{
        export class template {
            public load() : void {
                var $this = this;
                var defaulttemplate = new tsu.templates.defaulttemplate();
                $('#corestyles').append(defaulttemplate.styles);
                $('body').append(defaulttemplate.template);
            }
        }
    }
}