namespace tsu {
    export namespace engines{
        export class database {
            public db = localStorage;
            public get_config() : any {
                var config = {};
                config = this.db.getItem('config');
                return config;
            }
            public save_config() : void {
                var $this = this;
                var defaulttemplate = new tsu.templates.defaulttemplate();
                $('#corestyles').append(defaulttemplate.styles);
                $('body').append(defaulttemplate.template);
            }
        }
    }
}