namespace tsu {
    export namespace engines{
        export class map extends tsu.elements.aelement{
            public elementid = "#mapcanvas";
            public parentid = "#mapcontainer";
            public rooms = {};
            public canvas:any;
            public ctx:any;
            public zoom = 3;
            public currentid : string;
            public drawnexits : string[] = [];
            public generatedrooms : Object[] = [];
            public stepsize = 80;
            public width = 0;
            public height = 0;
            public half_width = 0;
            public half_height = 0;
            public step = 0;
            public size = 0;
            public half_size = 0; 
            public shortmap = [
                "nw"
                ,"n"
                ,"ne"
                ,"w"
                ,"e"
                ,"sw"
                ,"s"
                ,"se"
            ];
            public reversemap = {
                "nw":"se"
                ,"n":"s"
                ,"ne":"sw"
                ,"e":"w"
                ,"se":"nw"
                ,"s":"n"
                ,"sw":"ne"
                ,"w":"e"
            };
            clean(){
                var $this = this;
                $this.ctx.clearRect(0, 0, $this.width, $this.height); 
            }
            constructor(){
                super();
                var $this = this;
                $this.styles = `  
                
                    #terminalwrapper{
                        position: relative;
                        width: 100%;                        
                    }
                    #mapcontainer{
                        position:absolute;
                        max-width: 25%;
                        top:0;
                        right:0;
                        margin-right: 1%;
                        margin-top: 2%;
                    }    
                    #mapcanvas {
                        background-image: url(/images/map.jpg);
                        background-size: 100% 100%;
                        background-color: black;
                        max-width: 100%;
                    }
                    @media (min-width: 768px) { 
                        #mapcontainer{
                            margin-top: 0%;
                        }    
                    }
                
                    /*          
                    #mapcanvas {
                        background-image: url(/images/map.jpg);
                        background-size: 100% 100%;
                        background-color: black;
                        margin-top: 5px;
                        margin-left: 15px;
                        max-width: 100%;
                    }
                    #mapcontainer{
                        min-width: 25%;
                        max-width: 25%;
                        display: none;
                    }         
                    #terminalwrapper{
                        min-width: 100%;
                        max-width: 100%;
                    }
                    @media (min-width: 768px) { 
                        #mapcontainer {
                            display: block;
                        }
                        #terminalwrapper{
                            min-width: 73%;
                            max-width: 73%;
                        }
                    }
                    */
                `;
                //todo add reload room button, which refreshes room data
                //todo add remove room buttom, which deletes any references to the current room
                //todo add room viewer, which loads all rooms in the area, with a list of items, npc
                //todo --in a new engine-- add a search engine, that searches all inventory items, all rooms, all items in rooms, etc -- with path finding capabilities
                $this.template = `
                    <canvas id="mapcanvas"></canvas>
                `;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.room
                    ,func:function(obj){
                        setTimeout(function(){                            
                            var newroom = $this.addroom(obj);
                            $this.drawnexits = [];
                            $this.generatedrooms = [];
                            if(obj.id == $this.currentid) return;
                                $this.currentid = obj.id;
                            
                            //console.log(obj.id);

                            //todo add cache

                            $this.build(true,obj.areaid, obj.id.toString(), null, null, null, false, null);
                            $this.generatedrooms = $this.generatedrooms.sort(function ( a:any, b:any ) {
                                return (a.row - b.row || a.column - b.column || a.room.exits.length - b.room.exits.length || a.id.length - b.id.length);
                                //return (a.row - b.row || a.column - b.column || a.id.length - b.id.length);
                            });
                            var topleft;
                            //find first drawn room
                            for(var i=0; i < $this.generatedrooms.length; i++){
                                if( (<any>$this.generatedrooms[i]).draw ) {
                                    topleft = $this.generatedrooms[i];
                                    break;
                                }
                            }
                            //console.log($this.generatedrooms);
                            $this.drawnexits = [];
                            $this.generatedrooms = [];
                            $this.build(true, obj.areaid, topleft.id.toString(), 0, 0, null, false, topleft.room);
                            $this.generatedrooms.forEach((r:any)=>r.id == obj.id ? r.active = true : false);
                            $this.generatedrooms = $this.generatedrooms.sort(function ( a:any, b:any ) {
                                return (a.row - b.row || a.column - b.column || a.room.exits.length - b.room.exits.length || a.id.length - b.id.length);
                                //return (a.row - b.row || a.column - b.column || a.id.length - b.id.length);
                            });

                            $this.shift();
                            $this.draw();
                        },1);
                    }
                });
            }
            public after_database(){
                var $this = this;
                var s = $this.db.getItem("map");
                if(!s) return;
                $this.rooms = JSON.parse(s);                
            }
            public addroom(obj) : boolean{
                var $this = this;
                var flag = false;
                if(!$this.rooms.hasOwnProperty(obj.areaid)) $this.rooms[obj.areaid] = {};
                var realm = $this.rooms[obj.areaid];
                if(!realm.hasOwnProperty(obj.id)) {
                    realm[obj.id] = obj;
                    $this.db.setItem("map",JSON.stringify($this.rooms));
                    flag = true;
                }               
                return flag; 
            }
            public after_load(){
                var $this = this;
                $this.canvas = document.getElementById("mapcanvas");
                $this.ctx = $this.canvas.getContext("2d");   
                $this.canvas.addEventListener('click', function(evt) {
                    var mousePos = $this.getMousePos($this.canvas, evt);
                }, false);
                $this.setsizes();           
            }
            public setsizes(){
                var $this = this;
                $this.width = $this.canvas.width;
                $this.height = $this.canvas.height;
                $this.half_width = $this.width/2;
                $this.half_height = $this.height/2;
                $this.step = $this.stepsize/$this.zoom;
                $this.size = $this.step/4;
                $this.half_size = $this.size/2;  
            }
            public buildarray() : Object[] {
                return [];
            }    
            public shift() : void {
                var $this = this;
                var rerun = false;  
                
                var ordered = $this.generatedrooms;
                
                for(var i = 0; i < ordered.length; i++){ //exclude our first room
                    var checkroom = <any>ordered[i];
                    //if(!checkroom.draw) continue;                   
                    for(var j = 0; j < ordered.length; j++){ //exclude our first room                           
                        if(j == i) continue; //we don't want to check ourselves
                        //check of my x && y conflicts with another location
                        var testroom = <any>ordered[j];
                        //if(!testroom.draw) continue; 
                        if(checkroom.column != testroom.column || checkroom.row != testroom.row) continue;
                        if(!checkroom || !testroom) continue;
                        if(!checkroom.dir && !testroom.dir) continue;
                        
                        var dontmove = j; 
                        var workingroom = testroom;
                        if(!workingroom.dir) {
                            //console.log( Object.keys(workingroom.room.exits) );
                            workingroom.dir = Object.keys(workingroom.room.exits)[0];
                        }
                        
                        rerun = true;
                        var column = checkroom.column;
                        var row = checkroom.row;                        
                        var shiftleft = false;
                        var shiftright = false;
                        var shiftup = false;
                        var shiftdown = false;
                        var offsetx = 0;
                        var offsety = 0;                       

                        /*
                            take the 2 rooms, and determine best way to shift away from each other                            
                        */
                        var rev = workingroom.dir; //$this.reversemap[workingroom.dir];

                        //todo shift one direction, record it, and if we have a second pass, shift the second direction
                        if(rev.includes("n")) { offsety -= 1; shiftup = true; }
                        if(rev.includes("s")) { offsety += 1; shiftdown = true; }
                        if(rev.includes("e")) { offsetx += 1; shiftright = true; }
                        if(rev.includes("w")) { offsetx -= 1; shiftleft = true; }                   

                        //console.log(checkroom, testroom, i, j, dontmove, offsetx, offsety);
                        //offsety *= -1;
                        //offsetx *= -1;

                        //w or n shift already drawn maps, e or s shift maps we haven't drawn, both expand outwardly to make this a clearing
                        var from = 0;
                        var to = $this.generatedrooms.length;       
                        
                        //manually shift i
                        (<any>checkroom).column += offsetx;
                        (<any>checkroom).row += offsety;

                        //perform an after test to see if this intersects with another line, or has a conflict
                        //if so multiply offsets
                        // for(var k = from; k < to; k++) {                        
                        //     if(k == i) continue;
                        //     offsetx *= 2;
                        //     offsety *= 2;
                            // var pathtestroom = <any>ordered[k];
                            // if(pathtestroom.row == checkroom.row && pathtestroom.column == checkroom.column) {
                            //     offsetx *= 2;
                            //     offsety *= 2;
                            //     break;
                            // } else if(pathtestroom.row == checkroom.row-1) {
                            //     break;
                            // } else if(pathtestroom.column == checkroom.column) {

                            //     break;
                            // }
                        //}
                        
                        // if(shiftleft || shiftup){
                        //     to = j;
                        // } else if(shiftdown || shiftright){
                        //     from = i;  
                        // }

                        // } else if( (shiftleft || shiftup) && (shiftdown || shiftright) ){
                        //     //dontmove = i;
                        // } 

                        //loop everything shift outward
                        for(var k = from; k < to; k++) {
                            if(k == i || k == j) continue;

                            var shiftx = false;
                            var shifty = false;
                            var thisoffsetx = offsetx;
                            var thisoffsety = offsety;
                            //if( !(shiftleft || shiftup) && (shiftdown || shiftright) ) {
                                shiftx = (shiftleft && (<any>$this.generatedrooms[k]).column <= column)
                                shiftx = shiftx || (shiftright && (<any>$this.generatedrooms[k]).column >= column)

                                shifty = (shiftup && (<any>$this.generatedrooms[k]).row <= row)
                                shifty = shifty || (shiftdown && (<any>$this.generatedrooms[k]).row >= row)
                            // } else {
                            //     shiftx = true;
                            //     shifty = true;
                            //     thisoffsetx = (<any>$this.generatedrooms[k]).column <= column ? -1 : 1;
                            //     thisoffsety = (<any>$this.generatedrooms[k]).row <= row ? -1 : 1;
                            // }

                            if(shiftx) (<any>$this.generatedrooms[k]).column += thisoffsetx;
                            if(shifty) (<any>$this.generatedrooms[k]).row += thisoffsety;                        
                        }                         
                        break;
                     }
                    if(rerun) break;
                }

                if(rerun) $this.shift();

                //reloop
                //determine if my room intersects with a path, if so shift

                //reloop
                //if we have a south direction, and it is at the same row as us, shift it downward


            }


            public build(first:boolean, areaid:string, id:string, row:number, column:number, dir:string, includenodraw:any, currentroom?:any) : any {
                var $this = this; 
                var isactive = row == null && column == null;
                row = row || 0;
                column = column || 0; 

                //if(!$this.rooms[areaid].hasOwnProperty(id)) return;
                if($this.drawnexits.includes(id)) return;
                $this.drawnexits.push(id);

                currentroom = currentroom ? currentroom : $this.rooms[areaid][id];
                var thisroom = {
                    id:id
                    ,active: isactive //is my current room
                    ,row:row
                    ,draw:true
                    ,column:column
                    ,dir:dir
                    ,room:currentroom
                    ,otherexits:false
                    ,children:[]
                } 

                //draw our children rooms
                for(var exit in currentroom.exits){
                    //we only draw cardinal directions, anything else will open into another map
                    if(!$this.shortmap.includes(exit)) {
                        thisroom.otherexits = true;
                        continue;
                    }
                    var exitinfo = currentroom.exits[exit];
                    var thisrow = row;
                    var thiscolumn = column;
                    if(exit.includes("n")) {
                        thisrow -= 1;
                    }
                    if(exit.includes("s")) {
                        thisrow += 1;
                    }
                    if(exit.includes("e")) {
                        thiscolumn += 1;
                    }
                    if(exit.includes("w")) {
                        thiscolumn -= 1;
                    }   
                    var childroom = {
                        id:exitinfo.targetid
                        ,active: false
                        ,row:thisrow
                        ,draw:false
                        ,column:thiscolumn
                        ,dir:exit
                        ,otherexits:false
                        ,room:{
                            exits:{
                                [$this.reversemap[exit]]:{
                                    short: $this.reversemap[exit]
                                    ,targetid: id
                                }
                            }
                        }
                        ,children:[]
                    }
                    if($this.rooms[areaid].hasOwnProperty(exitinfo.targetid) && !$this.drawnexits.includes(exitinfo.targetid)){
                        childroom.draw = true;
                        childroom.room = $this.rooms[areaid][exitinfo.targetid];
                        for(var e in childroom.room.exits){
                            if(!$this.shortmap.includes(e)) {
                                childroom.otherexits = true;
                            }
                        }
                    }

                    //if this child does not contain an exit to the parent, do not draw it
                    if(childroom.draw) {   
                        var found = false;   
                        for(var e in childroom.room.exits) {                                 
                            if(!$this.shortmap.includes(e)) {
                                continue;
                            }
                            //hunt for the child room      
                            if(childroom.room.exits[e].targetid == id){
                                found = true;
                            }
                        }                       
                        if(!found) childroom.draw = false;
                    }

                    if(childroom.draw)
                        childroom.children = $this.build(false,areaid,childroom.id,childroom.row,childroom.column,childroom.dir,includenodraw,null) 

                    thisroom.children.push(childroom);
                    
                    if(childroom.draw || includenodraw) $this.generatedrooms.push(childroom)
                }    

                if(first) {
                    $this.generatedrooms.push(thisroom);
                    return thisroom;
                }

                return thisroom.children;
            }

            public draw() : void {
                var $this = this;
                $this.ctx.clearRect(0, 0, $this.width, $this.height); 

                //correct all columns based on the offset of my active room, so my active room will be in the center
                var correctx = 0;
                var correcty = 0;
                $this.generatedrooms.forEach(function(r:any){
                    if(r.active){
                        correctx = r.column;
                        correcty = r.row;
                    }
                });

                $this.generatedrooms.forEach(function(r:any){

                    //if(r.id == '/room/shop') console.log(r);

                    //update my immediate children so i know where to draw the line to
                    for(var i = 0; i < r.children.length; i++){ //exclude our first room
                        var mychild = <any>r.children[i];   
                        var found = false;            
                        for(var j = 0; j < $this.generatedrooms.length; j++){                            
                            var genroom = <any>$this.generatedrooms[j];
                            if(genroom.id == mychild.id){
                                mychild.row = genroom.row;
                                mychild.column = genroom.column;
                                r.children[i] = mychild;
                                found = true;
                            }
                        }                        
                        if(!found){
                            //correct the room not visible
                            var offsety = 0;
                            var offsetx = 0;
                            mychild.row = r.row;
                            mychild.column = r.column;
                            if(mychild.dir.includes("n")) { offsety -= 1; }
                            if(mychild.dir.includes("s")) { offsety += 1; }
                            if(mychild.dir.includes("e")) { offsetx += 1; }
                            if(mychild.dir.includes("w")) { offsetx -= 1; }  
                            mychild.row += offsety;
                            mychild.column += offsetx;                            
                            for(var k in $this.rooms){
                                var room = $this.rooms[k];
                                for(var ids in room){
                                    if(ids == mychild.id){
                                        mychild.text = k;
                                    }
                                }
                            }
                            r.children[i] = mychild;
                        }
                    }

                    if(r.active){
                        $this.ctx.fillStyle = "#FF0000";                        
                    } else {
                        $this.ctx.fillStyle = "#FFFFFF";
                        $this.ctx.strokeStyle = "#FFFFFF";
                        $this.ctx.font = "10px Arial";
                    }
                    
                    if(r.draw) {
                        var x = $this.half_width + (((r.column - correctx) * $this.step)-$this.half_size);
                        var y = $this.half_height + (((r.row - correcty) * $this.step)-$this.half_size);
                        $this.ctx.fillRect(x, y, $this.size, $this.size);
                    }
                    
                    //draw path
                    //draw a line from me to all of my children
                    r.children.forEach(function(c:any){
                        var coffsety = 0;
                        var coffsetx = 0;

                        if(c.dir.includes("n")) { coffsety -= $this.half_size; }
                        if(c.dir.includes("s")) { coffsety += $this.half_size; }
                        if(c.dir.includes("e")) { coffsetx += $this.half_size; }
                        if(c.dir.includes("w")) { coffsetx -= $this.half_size; } 

                        var x1 = $this.half_width + (((r.column - correctx) * $this.step)) + coffsetx;
                        var y1 = $this.half_height + (((r.row - correcty) * $this.step)) + coffsety;
                        var x2 = $this.half_width + (((c.column - correctx) * $this.step)) + (coffsetx*-1);
                        var y2 = $this.half_height + (((c.row - correcty) * $this.step)) + (coffsety*-1);

                        $this.ctx.beginPath();
                        $this.ctx.moveTo(x1, y1);
                        $this.ctx.lineTo(x2, y2);
                        $this.ctx.stroke();
                        if(c.text){     
                            var txt = c.text.substring(0,9);
                            $this.ctx.fillStyle = "#000000";
                            $this.ctx.font = "10px Arial";
                            $this.ctx.textAlign = "center";                        
                            $this.ctx.fillText(txt, x2, y2); 
                        }
                    });
                });
            }

            protected getMousePos(canvas, event) {
                var rect = canvas.getBoundingClientRect();
                return {
                    x: event.clientX - rect.left,
                    y: event.clientY - rect.top
                };
            }
            protected isInside(pos, rect){
                return pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.height && pos.y > rect.y
            }    
        }
    }
}