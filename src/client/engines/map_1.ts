namespace tsu {
    export namespace engines{
        export class map1 extends tsu.elements.aelement{
            public elementid = "#mapcanvas";
            public parentid = "#mapcontainer";
            public rooms = {};
            public canvas:any;
            public ctx:any;
            public zoom = 3;
            public currentid : string;
            public drawnexits : string[] = [];
            public generatedrooms : Object[] = [];
            public stepsize = 40;
            public width = 0;
            public height = 0;
            public half_width = 0;
            public half_height = 0;
            public step = 0;
            public size = 0;
            public half_size = 0; 
            public shortmap = [
                "nw"
                ,"n"
                ,"ne"
                ,"w"
                ,"e"
                ,"sw"
                ,"s"
                ,"se"
            ];
            public reversemap = {
                "nw":"se"
                ,"n":"s"
                ,"ne":"sw"
                ,"e":"w"
                ,"se":"nw"
                ,"s":"n"
                ,"sw":"ne"
                ,"w":"e"
            };
            constructor(){
                super();
                var $this = this;
                $this.styles = `
                    #mapcanvas {
                        background-color: black;
                    }
                `;
                //todo add reload room button, which refreshes room data
                //todo add remove room buttom, which deletes any references to the current room
                //todo add room viewer, which loads all rooms in the area, with a list of items, npc
                //todo --in a new engine-- add a search engine, that searches all inventory items, all rooms, all items in rooms, etc -- with path finding capabilities
                $this.template = `
                    <canvas id="mapcanvas"></canvas>
                `;
                $this.events.push(<elements.ievent>{
                    eventtype: enums.events.room
                    ,func:function(obj){
                        var newroom = $this.addroom(obj);
                        $this.drawnexits = [];
                        $this.generatedrooms = [];
                        if(obj.id == $this.currentid) return;
                            $this.currentid = obj.id;

                        //if(newroom || )
                        //if we have a cache use it to draw

                        $this.generatemap(obj.areaid, obj.id.toString());

                        // $this.build(obj.areaid, obj.id.toString(), null, null, null);
                        // $this.shift();
                        // $this.draw();

                        //add generatedrooms to cache

                        //$this.draw(obj.areaid, obj.id.toString(), null, null);
                        //console.log($this.generatedrooms);
                        //console.log(obj);
                        //console.log($this.drawnexits);
                    }
                });
            }
            public after_database(){
                var $this = this;
                var s = $this.db.getItem("map");
                if(!s) return;
                $this.rooms = JSON.parse(s);                
            }
            public addroom(obj) : boolean{
                var $this = this;
                var flag = false;
                if(!$this.rooms.hasOwnProperty(obj.areaid)) $this.rooms[obj.areaid] = {};
                var realm = $this.rooms[obj.areaid];
                if(!realm.hasOwnProperty(obj.id)) {
                    realm[obj.id] = obj;
                    $this.db.setItem("map",JSON.stringify($this.rooms));
                    flag = true;
                }               
                return flag; 
            }
            public after_load(){
                var $this = this;
                $this.canvas = document.getElementById("mapcanvas");
                $this.ctx = $this.canvas.getContext("2d");   
                $this.canvas.addEventListener('click', function(evt) {
                    var mousePos = $this.getMousePos($this.canvas, evt);
                }, false);
                //$this.draw();  
                $this.setsizes();           
            }
            public setsizes(){
                var $this = this;
                $this.width = $this.canvas.width;
                $this.height = $this.canvas.height;
                $this.half_width = $this.width/2;
                $this.half_height = $this.height/2;
                $this.step = $this.stepsize/$this.zoom;
                $this.size = $this.step/2;
                $this.half_size = $this.size/2;  
            }
            public buildarray() : Object[] {
                return [];
            }
            public shift(direction:string) : void {
                var $this = this;    
            }
            public generatemap(areaid:string, id:string) : any {
                var $this = this;
                /*
                    we want to build an initial map, to find all connecting maps
                    order that to find the lowest x,y,name
                    
                    now we have a normalized array to build from

                    loop over the array and use a corrected row/column, starting from 0,0
                    record current x,y locations
                    if the x,y location was already recorded
                        change my position based on my exit weight
                            exit weight giving a value for cardinal positions, nw, w, sw would be 3 for west
                                nw would be 1n 1w for weights
                            take these weights and attempt to shift south and east? because that is the direction we are drawing the map?
                        update all of my children with a position that would be impacted by my change
                */
                var map = $this.build(areaid, id, null, null, null);
                var ordered = map.sort(function ( a:any, b:any ) {
                    return (a.row - b.row || a.column - b.column || a.id.length - b.id.length);
                });
                var alreadyfound = [];
                for(var i = 0; i<ordered.length; i++){
                    var thismap = ordered[i];


                    //alreadyfound
                }                
            }
            public getexitweights(dir:string){
                var weights = {

                };
                return weights;
            }
            public build(areaid:string, id:string, row:number, column:number, dir:string) : any {
                var $this = this; 
                var isactive = row == null && column == null;
                row = row || 0;
                column = column || 0;               

                if($this.drawnexits.includes(id)) return;
                $this.drawnexits.push(id);

                var currentroom = $this.rooms[areaid][id];
                var thisroom = {
                    id:id
                    ,active: isactive //is my current room
                    ,row:row
                    ,draw:true
                    ,column:column
                    ,dir:dir
                    ,room:currentroom
                    ,children:[]
                } 

                //draw our children rooms
                for(var exit in currentroom.exits){
                    //we only draw cardinal directions, anything else will open into another map
                    if(!$this.shortmap.includes(exit)) continue;
                    var exitinfo = currentroom.exits[exit];
                    var thisrow = row;
                    var thiscolumn = column;
                    if(exit.includes("n")) {
                        thisrow -= 1;
                    }
                    if(exit.includes("s")) {
                        thisrow += 1;
                    }
                    if(exit.includes("e")) {
                        thiscolumn += 1;
                    }
                    if(exit.includes("w")) {
                        thiscolumn -= 1;
                    }   
                    var childroom = {
                        id:exitinfo.targetid
                        ,active: false
                        ,row:thisrow
                        ,draw:false
                        ,column:thiscolumn
                        ,dir:exit
                        ,room:null
                        ,children:[]
                    }
                    if($this.rooms[areaid].hasOwnProperty(exitinfo.targetid) && !$this.drawnexits.includes(exitinfo.targetid)){
                        childroom.draw = true;
                        childroom.room = $this.rooms[areaid][exitinfo.targetid];
                    }
                    if(childroom.draw)
                        childroom.children = $this.build(areaid,childroom.id,childroom.row,childroom.column,childroom.dir) 
                    
                    thisroom.children.push(childroom);
                }    

                $this.generatedrooms.push(thisroom);

                return thisroom.children;
            }

            public draw() : void {
                var $this = this;

                $this.ctx.clearRect(0, 0, $this.width, $this.height); 
                $this.generatedrooms.forEach(function(r:any){

                    //update my immediate children so i know where to draw the line to
                    for(var i = 0; i <= r.children.length-1; i++){ //exclude our first room
                        var mychild = <any>r.children[i];                 
                        for(var j = 0; j <= $this.generatedrooms.length-1; j++){                            
                            var genroom = <any>$this.generatedrooms[j];
                            if(genroom.id == mychild.id){
                                mychild.row = genroom.row;
                                mychild.column = genroom.column;
                            }
                        }
                    }

                    if(r.dir == null){
                        $this.ctx.fillStyle = "#FF0000";                        
                    } else {
                        $this.ctx.fillStyle = "#FFFFFF";
                        $this.ctx.strokeStyle = "#FFFFFF";
                    }

                    
                    if(r.draw) {
                        var x = $this.half_width + ((r.column * $this.step)-$this.half_size);
                        var y = $this.half_height + ((r.row * $this.step)-$this.half_size);
                        $this.ctx.fillRect(x, y, $this.size, $this.size);
                        //console.log(x,y);
                    }
                    
                    //draw path
                    //draw a line from me to all of my children
                    r.children.forEach(function(c:any){
                        var x1 = $this.half_width + ((r.column * $this.step)-$this.half_size);
                        var y1 = $this.half_height + ((r.row * $this.step)-$this.half_size);
                        var x2 = $this.half_width + ((c.column * $this.step)-$this.half_size);
                        var y2 = $this.half_height + ((c.row * $this.step)-$this.half_size);
                        $this.ctx.beginPath();
                        $this.ctx.moveTo(x1, y1);
                        $this.ctx.lineTo(x2, y2);
                        $this.ctx.stroke();
                    });
                });
            }
            protected getMousePos(canvas, event) {
                var rect = canvas.getBoundingClientRect();
                return {
                    x: event.clientX - rect.left,
                    y: event.clientY - rect.top
                };
            }
            protected isInside(pos, rect){
                return pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.height && pos.y > rect.y
            }    
        }
    }
}