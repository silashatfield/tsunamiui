FROM node:latest

RUN mkdir /tsunami && mkdir /tsunami/build && mkdir /tsunami/styles && mkdir /tsunami/images

COPY package.json       /tsunami/
COPY index.html         /tsunami/
COPY styles/            /tsunami/styles/
COPY images/            /tsunami/images/
COPY build/server.js    /tsunami/build/

RUN cd /tsunami && npm install
WORKDIR /tsunami/build

ENTRYPOINT node 
CMD server.js


